function g = preProcGraph(ingraph, options)
% graph = preProcGraph(swc_graph, params)
% 
% Calculates the representation of the graph from a graph extracted from
% SWC files.
% 
% swc_graph - graph extracted from SWC files
% params    - parameters for the GMMC method
% graph     - graph struct
% 
%

if nargin<2
    options = get_gmmc_params(options);
end

D=2;
if sum(abs(ingraph.Z))>0, D=3; end;

% pts = [union(find(sum(ingraph.dA,2)==0)', find(sum(ingraph.dA)>1)) find(sum(ingraph.dA)==0)];

cpoints = C_tree(ingraph);
cpoints(sum(ingraph.dA,2)==0) = 0; % not the root
if ~isempty(find(cpoints, 1))
    bpgraph = delete_tree (ingraph, find(cpoints));
else
    bpgraph = ingraph;
end

X = [ingraph.X ingraph.Y];
bppts = [bpgraph.X bpgraph.Y];
if D==3, X=[X ingraph.Z]; bppts=[bppts bpgraph.Z]; end

dA = sparse(bpgraph.dA);
ipar = ipar_tree (ingraph);
bp = BP_tree(ingraph,1);

pts = zeros(1, size(bppts,1));
for i=1:length(pts)
    pt = bp(all(repmat(bppts(i,:), length(bp),1)==X(bp,:),2));
    if length(pt)>1
        X(pt(1),1) = X(pt(1),1)+eps;
        ingraph.X(pt(1)) = X(pt(1),1);
        pts(i) = pt(1);
    else
        pts(i) = pt;
    end
end
g.nodes = [ingraph.X(pts) ingraph.Y(pts)];
if D==3, g.nodes=[g.nodes ingraph.Z(pts)]; end

cnt=1;
if strcmp(options.interpolfun,'bspline')
    g.edges(size(dA,1)).form = '';
    g.edges(size(dA,1)).breaks = [];
    g.edges(size(dA,1)).coefs = [];
    g.edges(size(dA,1)).pieces = [];
    g.edges(size(dA,1)).order = [];
    g.edges(size(dA,1)).dim = [];
    g.edges(size(dA,1)).endpoints = [];
    g.edges(size(dA,1)).length = [];
    for i=1:size(dA,1)
        j = find(dA(i,:));
        if isempty(j)
            g.edges(end)=[];
            continue;
        end
        pathpts = ipar  (pts(i), 1 : find (ipar (pts(i), :) == pts(j) ));
    %     plot(ingraph.X(path), ingraph.Y(path), '.', 'Color', rand(1,3));
        path = X(pathpts(end:-1:1),:)';
        s = cumsum([0 sqrt(sum(diff(path,[],2).^2, 1))])';
        pp = splinefit(s,path,min(2,length(s)-1),min(options.spo,max(length(s)-1,2)));
        pp.endpoints = [j i];
        pp.length = s(end);

        g.edges(cnt) = pp; cnt=cnt+1;
    end
elseif strcmp(options.interpolfun,'linear')
%     g.edges(size(dA,1)).dim = [];
    g.edges(size(dA,1)).endpoints = [];
    g.edges(size(dA,1)).path = [];
    g.edges(size(dA,1)).cumd = [];
    g.edges(size(dA,1)).length = [];
    for i=1:size(dA,1)
        j = find(dA(i,:));
        if isempty(j)
            g.edges(end)=[];
            continue;
        end
        pathpts = ipar  (pts(i), 1 : find (ipar (pts(i), :) == pts(j) ));
    %     plot(ingraph.X(path), ingraph.Y(path), '.', 'Color', rand(1,3));
        pp.endpoints = [j i];
        pp.path = X(pathpts(end:-1:1),:)';
        pp.cumd = cumsum([0 sqrt(sum(diff(pp.path,[],2).^2, 1))]);
        pp.length = pp.cumd(end);

        g.edges(cnt) = pp; cnt=cnt+1;
    end
end
g.dA = dA;

end
