function p = linear_zetaedge(g, id, t)

path = g.edges(id).path;
cumd = g.edges(id).cumd;

%     tpath = [tpath path];

[rows,inds] = find(and(bsxfun(@le, g.edges(id).cumd(1:end-1), (t.*g.edges(id).length)'), ...
    bsxfun(@ge, g.edges(id).cumd(2:end), (t.*g.edges(id).length)')));
[~,is] = unique(rows);
inds = inds(is);
div = cumd(inds+1)-cumd(inds);
div(div==0) = 1;
p = path(:,inds) + (path(:,inds+1)-path(:,inds)).*...
    repmat((t.*g.edges(id).length-cumd(inds))./div, size(g.nodes,2), 1);

end