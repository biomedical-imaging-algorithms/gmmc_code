function [Xp,cov2] = mygp(tX_A, tX_B, X_A, thetas, betainv)
% [Xp,cov2] = mygp(tX_A, tX_B, X_A, thetas, betainv)

% load ~/Documents/treematching/pcmatching/Toy/simpleexample.mat;
% % load ~/Documents/synth_datasets/generic/tree000.mat;
% % X_A=X;
% % X_B=Y;
% % X_A = [X_A X_A];
% 
% GP_NOISE=.0025; GP_FLEX=0.1; GP_SCALE=.25;
% 
% tX_A = X_A([1 2 3],:);
% tX_B = X_B([4 3 2],:);

Ks = (thetas(1) + thetas(2)*tX_A*X_A' + thetas(3)*exp( -1/2 * pdist2(tX_A,X_A).^2 * thetas(4) ))';
C =  (thetas(1) + thetas(2)*(tX_A*tX_A') + thetas(3)*exp( -1/2 * pdist2(tX_A,tX_A).^2 * thetas(4) ))' + eye(size(tX_A,1),size(tX_A,1))*betainv^2;
Xp = Ks/C*tX_B;
cov2 = thetas(1) + thetas(2)*diag(X_A*X_A') + thetas(3)*exp( -1/2 *sum(( X_A-X_A ).^2, 2) * thetas(4) ) + betainv^2 - diag(Ks/C*Ks');


end