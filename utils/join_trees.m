function outree = join_trees(tree1, tree2)

outree.X = [tree1.X; tree2.X];
outree.Y = [tree1.Y; tree2.Y];
outree.Z = [tree1.Z; tree2.Z];
outree.R = [tree1.R; tree2.R];
outree.D = [tree1.D; tree2.D];

outree.dA = [tree1.dA zeros(size(tree1.dA,1), size(tree2.dA,2));...
            zeros(size(tree2.dA,1),size(tree1.dA,2)), tree2.dA];

end