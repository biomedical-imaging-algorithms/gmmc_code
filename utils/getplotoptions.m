function opt = getplotoptions(opt)

if ~isfield(opt, 'showid')
    opt.showid = false;
end
if ~isfield(opt, 'color')
    opt.color = [0 0 1];
end
if ~isfield(opt, 'sampling')
    opt.sampling = .05;
end
if ~isfield(opt, 'text')
    opt.text = true;
end
if ~isfield(opt, 'linewidth')
    opt.linewidth = 2;
end
if ~isfield(opt, 'markersize')
    opt.markersize = 20;
end
if ~isfield(opt, 'display_virtual')
    opt.display_virtual = true;
end

end