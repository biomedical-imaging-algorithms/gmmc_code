function plot2gs (a, b, opt, Mv, Ms, onlyA, onlyB)

if nargin<7
    onlyB=[];
end
if nargin<6
    onlyA=[];
end
if nargin<5
    Ms=[];
end
if nargin<4
    Mv=[];
end
if nargin<3
    opt=[];
end

% opt.text=1;

if ~isfield(opt, 'desvA')
    opt.desvA = [0 0];
end
if ~isfield(opt, 'desvB')
    opt.desvB = [2.5 0];
end
if ~isfield(opt, 'color1')
    opt.color1 = [0 0 1];
end
if ~isfield(opt, 'color2')
    opt.color2 = [0 .7 0];
end

a.nodes(:,1) = a.nodes(:,1)+opt.desvA(1);
a.nodes(:,2) = a.nodes(:,2)+opt.desvA(2);
b.nodes(:,1) = b.nodes(:,1)+opt.desvB(1);
b.nodes(:,2) = b.nodes(:,2)+opt.desvB(2);

for i=1:length(a.edges)
    a.edges(i).path(1,:) = a.edges(i).path(1,:)+opt.desvA(1);
    a.edges(i).path(2,:) = a.edges(i).path(2,:)+opt.desvA(2);
end
for i=1:length(b.edges)
    b.edges(i).path(1,:) = b.edges(i).path(1,:)+opt.desvB(1);
    b.edges(i).path(2,:) = b.edges(i).path(2,:)+opt.desvB(2);
end

if ~isempty(Mv)
    hold on;
    if size(a.nodes,2)==2
        for i=1:size(Mv,2)
            if Mv(1,i)~=-1 && Mv(2,i)~=-1
            plot(a.nodes(Mv(1,i),1), a.nodes(Mv(1,i),2), '.', 'MarkerSize', 40, 'Color', [.8 .8 0]);
            plot(b.nodes(Mv(2,i),1), b.nodes(Mv(2,i),2), '.', 'MarkerSize', 40, 'Color', [.8 .8 0]);
            plot([a.nodes(Mv(1,i),1) b.nodes(Mv(2,i),1)],...
                [a.nodes(Mv(1,i),2) b.nodes(Mv(2,i),2)], ...
                'Color', [.8 .8 0], 'LineWidth', 1.2);
            end
        end
    else
        for i=1:size(Mv,2)
            if Mv(2,i)~=-1 && Mv(1,i)~=-1
            plot3(a.nodes(Mv(1,i),1), a.nodes(Mv(1,i),2), a.nodes(Mv(1,i),3), '.', 'MarkerSize', 30, 'Color', [.8 .8 0]);
            plot3(b.nodes(Mv(2,i),1), b.nodes(Mv(2,i),2), b.nodes(Mv(2,i),3), '.', 'MarkerSize', 30, 'Color', [.8 .8 0]);
            plot3([a.nodes(Mv(1,i),1) b.nodes(Mv(2,i),1)],...
                [a.nodes(Mv(1,i),2) b.nodes(Mv(2,i),2)],...
                [a.nodes(Mv(1,i),3) b.nodes(Mv(2,i),3)], ...
                'Color', [.8 .8 0], 'LineWidth', 1.2);
            end
        end
    end
    hold off;
end

opt.color = opt.color1;
plotg(a,opt,onlyA);
opt.color = opt.color2;
plotg(b,opt,onlyB);

opt = getplotoptions(opt);
opt.colorMs = [.7 .1 0];
if ~isempty(Ms)
    hold on;
    if size(a.nodes,2)==2
        for i=1:size(Ms,2)
            if a.superedges(Ms(1,i)).edgeid(1)==0 || b.superedges(Ms(2,i)).edgeid(1)==0
                continue;
            end
            
            if isfield(a.edges(1), 'cumd')
                e = linear_zetaedge(a, Ms(1,i), 0:opt.sampling:1);
            else
                e = zetaedge(a, Ms(1,i), 0:opt.sampling:1);
            end
            plot(e(1,:), e(2,:), '-', 'Color', opt.colorMs,'LineWidth', opt.linewidth + .5);
            if isfield(b.edges(1), 'cumd')
                e = linear_zetaedge(b, Ms(2,i), 0:opt.sampling:1);
            else
                e = zetaedge(b, Ms(2,i), 0:opt.sampling:1);
            end
            plot(e(1,:), e(2,:), '-', 'Color', opt.colorMs,'LineWidth', opt.linewidth + .5);
        end
    else
        for i=1:size(Ms,2)
            if a.superedges(Ms(1,i)).edgeid(1)==0 || b.superedges(Ms(2,i)).edgeid(1)==0
                continue;
            end
            
            colorMs = rand(1,3);
            if isfield(a.edges(1), 'cumd')
                e = linear_zetasuperedge(a, Ms(1,i), 0:opt.sampling:1);
            else
                e = zetasuperedge(a, Ms(1,i), 0:opt.sampling:1);
            end
            plot3(e(1,:), e(2,:), e(3,:), '-', 'Color', colorMs,'LineWidth', opt.linewidth + 1);
            
            if isfield(b.edges(1), 'cumd')
                e = linear_zetasuperedge(b, Ms(2,i), 0:opt.sampling:1);
            else
                e = zetasuperedge(b, Ms(2,i), 0:opt.sampling:1);
            end
            plot3(e(1,:), e(2,:), e(3,:), '-', 'Color', colorMs,'LineWidth', opt.linewidth + 1);
        end
    end
    hold off;
end

end