function pts = BP_tree(tree, leaves)

if nargin<2
    leaves = true;
end

if leaves
    pts = unique([find(sum(tree.dA~=0)>1) find(sum(tree.dA~=0)==0) find(sum(tree.dA~=0,2)==0)']);
else
%     pts = unique([find(sum(tree.dA~=0)>1) find(sum(tree.dA~=0,2)==0)']);
    pts = find(sum(tree.dA~=0)>1);
end

end