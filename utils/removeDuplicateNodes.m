function A = removeDuplicateNodes(A, min_perc)

if nargin<2
    min_perc = .002;
end

max_dist = sqrt(sum((max(A.nodes) - min(A.nodes)).^2));
mindist = min_perc * max_dist;

oldind = 1:size(A.nodes,1);
dists = pdist2(A.nodes,A.nodes);
inval = tril(ones(size(dists)));
inval(inval==1)=Inf;
dists = dists + inval;

% Remove nodes corresponding to the same point
[a,b] = find(dists<=mindist);
[rmnodes,ind] = unique(b);
shouldbe = a(ind);

A.nodes(rmnodes,:) = [];
oldind(rmnodes) = [];
for i=1:length(A.edges)
    for j=1:length(A.edges(i).endpoints)
        k = find(oldind==A.edges(i).endpoints(j));
        if isempty(k)
            k = find(rmnodes==A.edges(i).endpoints(j));
            sh = find(oldind==shouldbe(k));
            while isempty(sh)
                k = find(rmnodes==shouldbe(k));
                sh = find(oldind==shouldbe(k));
            end
            A.edges(i).endpoints(j) = sh;
            continue;
        end
        A.edges(i).endpoints(j) = k;
        if length(A.edges(i).endpoints)~=2
            error('length(A.edges(i).endpoints)~=2');
        end
    end
end

for i=length(A.edges):-1:1
    if A.edges(i).endpoints(1)==A.edges(i).endpoints(end)
        A.edges(i) = [];
    end
end

A.dA = zeros(size(A.nodes,1));
for i=1:length(A.edges)
    A.dA(A.edges(i).endpoints(1),A.edges(i).endpoints(2)) = 1;
end
A.dA = sparse(min(A.dA+A.dA',1));