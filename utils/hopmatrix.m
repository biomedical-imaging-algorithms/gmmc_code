function [hm,paths] = hopmatrix(am)

hm = Inf(size(am));
paths = cell(size(am));
for i=1:size(am,1)
    [hm(:,i), nodes] = graphshortestpath(am, i, 'Directed', false);
    for j=1:length(nodes)
        if ~iscell(nodes(j))
            nodes = {nodes};
        end
        paths(i,j) = nodes(j);
    end
    progressbar(i,size(am,1),30);
end

end
