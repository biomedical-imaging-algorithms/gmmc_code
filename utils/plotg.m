function plotg(g, opt, only_pt)
% Possible options
%       color:    Color of the graph
%       sampling: Sampling along the edges
%       text:     Show node id {0,1}

if nargin<3
    only_pt = [];
end
if nargin<2
    opt = [];
end

opt = getplotoptions(opt);
pts_in_edge=[];

if size(g.nodes,2)==2
    
    hold on;
    vecol = min(1,opt.color+.7);
    if isfield(g,'superedges') && opt.display_virtual
        for i=1:length(g.superedges)
            if ~isempty(only_pt) && ~any(g.superedges(i).path==only_pt), continue; end;
            if g.superedges(i).edgeid(1)==0
                e = bsxfun(@plus, g.nodes(g.superedges(i).path(1),:), bsxfun(@times, g.nodes(g.superedges(i).path(end),:)-g.nodes(g.superedges(i).path(1),:), (0:opt.sampling:1)'))';
                plot(e(1,:), e(2,:), '-', 'Color', vecol,'LineWidth', opt.linewidth);
            end
        end
    end
    plot(g.nodes(:,1), g.nodes(:,2), '.', 'Color', opt.color,'MarkerSize',opt.markersize);
    for i=1:length(g.edges)
        if ~isempty(only_pt) && ~any(g.edges(i).endpoints==only_pt), continue; end;
        if isfield(g.edges(1), 'cumd')
            e = linear_zetaedge(g, i, 0:opt.sampling:1);
        else
            e = zetaedge(g, i, 0:opt.sampling:1);
        end
        plot(e(1,:), e(2,:), '-', 'Color', opt.color,'LineWidth', opt.linewidth);
    end
    if opt.text
        if isfield(g, 'pts')
            for i=1:size(g.pts,1)
                text(g.pts(i,1)+.01, g.pts(i,2), num2str(i),'FontSize',16,'FontWeight','bold','Color', opt.color)
            end
        else
            for i=1:size(g.nodes,1)
                text(g.nodes(i,1)+.01, g.nodes(i,2), num2str(i),'FontSize',16,'FontWeight','bold','Color', opt.color)
            end
        end
    end
    hold off;

elseif size(g.nodes,2)==3
    hold on;
    for i=1:length(g.edges)
        if ~isempty(only_pt) && ~any(g.edges(i).endpoints==only_pt), continue; end;
        if ~isempty(only_pt), pts_in_edge = [pts_in_edge g.edges(i).endpoints]; end;
        if isfield(g.edges(1), 'cumd')
            e = linear_zetaedge(g, i, 0:opt.sampling:1);
        else
            e = zetaedge(g, i, 0:opt.sampling:1);
        end
        plot3(e(1,:), e(2,:), e(3,:), '-', 'Color', opt.color,'LineWidth', opt.linewidth);
    end
    vecol = min(1,opt.color+.7);
    for i=1:length(g.superedges)
        if ~isempty(only_pt) && ~any(g.superedges(i).path==only_pt), continue; end;
        if ~isempty(only_pt), pts_in_edge = [pts_in_edge g.superedges(i).path]; end;
        if g.superedges(i).edgeid(1)==0 && opt.display_virtual
            e = bsxfun(@plus, g.nodes(g.superedges(i).path(1),:), bsxfun(@times, g.nodes(g.superedges(i).path(end),:)-g.nodes(g.superedges(i).path(1),:), (0:opt.sampling:1)'))';
            plot3(e(1,:), e(2,:), e(3,:), '-', 'Color', vecol,'LineWidth', opt.linewidth);
        end
    end
    if isempty(only_pt)
        plot3(g.nodes(:,1), g.nodes(:,2), g.nodes(:,3), '.', 'Color', opt.color,'MarkerSize',opt.markersize);
    else
        plot3(g.nodes(pts_in_edge,1), g.nodes(pts_in_edge,2), g.nodes(pts_in_edge,3), '.', 'Color', opt.color,'MarkerSize',opt.markersize);
    end
    if opt.text
        if isfield(g, 'pts')
            for i=1:size(g.pts,1)
                text(g.pts(i,1)+.02, g.pts(i,2), g.pts(i,3), num2str(i),'FontSize',16,'FontWeight','bold','Color', opt.color)
            end
        else
            list = 1:size(g.nodes,1);
            if ~isempty(only_pt), list = pts_in_edge; end;
            for i=list
                text(g.nodes(i,1)+.02, g.nodes(i,2), g.nodes(i,3), num2str(i),'FontSize',16,'FontWeight','bold','Color', opt.color)
            end
        end
    end
    hold off;
end

end