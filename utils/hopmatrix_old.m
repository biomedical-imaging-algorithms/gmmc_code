function [hm,paths] = hopmatrix_old(am, K)

if nargin<2
    K = Inf;
end

if ~all(all(am==am.'))
    am = am+am';
end
hm = full(am);
ey = eye(size(am));
paths = cell(size(am));

for i=1:size(am,1)
    walk = am(i,:);
    twalk = walk;
    added = walk;
    dist=2;
    thispath = cell(1,size(am,2));
    for j=1:length(thispath)
        if am(i,j)==1
            thispath{j} = [i j];
        end
    end
    while any(twalk(setdiff(1:size(hm,2), i))==1) && any(added(added>0)<2) && dist-1<K
        twalk = walk*am;
        for j=1:length(twalk)
            if twalk(j)==1 && (isempty(thispath{j}) || thispath{j}(end)~=j)
                thispath{j} = [thispath{find(walk'+am(:,j)==2)} j]; %#ok<FNDSB>
            end
        end
        walk = (walk*am == 1);
        toadd = dist.*walk;
        hm(i,hm(i,:)==0) = hm(i,hm(i,:)==0)+toadd(hm(i,:)==0);
        added = added+twalk;
        dist = dist+1;
    end
    hm(i,i) = 0;
    thispath{i} = [];
    paths(i,:) = thispath;
    hm(i,hm(i,:)==0 - ey(i,:)) = Inf;
    progressbar(i,size(am,1),30);
end

end