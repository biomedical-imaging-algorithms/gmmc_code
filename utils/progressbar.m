function progressbar(n,N,w,t)

% progressbar - display a progress bar
%
%    progressbar(n,N,w);
%
% displays the progress of n out of N.
% n should start at 1.
% w is the width of the bar (default w=20).
%
%   Copyright (c) Gabriel Peyre 2006

if nargin<4
    t=0;
end
if nargin<3
    w = 20;
end

global pprev;
if isempty(pprev)
    pprev = -1;
end

if nargin<2
    pprev = -1;
    return;
end

% progress char
cprog = '.';
cprog1 = '*';
% begining char
cbeg = '[';
% ending char
cend = ']';

p = min( floor(n/N*(w+1)), w);
digs = ceil(log10(N+1));

ptext=[];
if t, ptext = [' (' num2str(n,['%0' num2str(digs) 'd']) '/' num2str(N,['%0' num2str(digs) 'd']) ')']; end;
if not(p==pprev)
    ps = repmat(cprog, [1 w]);
    ps(1:p) = cprog1;
    ps = [cbeg ps cend ptext];
    if n>1 && pprev~=-1
        % clear previous string
        fprintf( repmat('\b', [1 length(ps)]) );
    end
    fprintf(ps);
elseif t==1
    if n>1 && pprev~=-1
        % clear previous string
        fprintf( repmat('\b', [1 length(ptext)]) );
    end
    fprintf(ptext);
end
pprev = p;
if n==N
    fprintf('\n');
end