function p = linear_zetasuperedge(g, id, t)


lens = [g.edges(abs(g.superedges(id).edgeid)).length];
s = cumsum([0 lens]);
s2 = s./s(end);
lens2 = diff(s2);
interval = sum(bsxfun(@ge, t', repmat(s2(1:end-1),length(t),1)),2)';
newt = (t-s2(interval))./lens2(interval);

p = zeros(size(g.nodes,2), length(t));
% tpath = [];
for i=1:length(g.superedges(id).edgeid)
    thist = newt(interval==i);
    if g.superedges(id).edgeid(i)<0, thist = 1-thist; end;
    
    path = g.edges(abs(g.superedges(id).edgeid(i))).path;
    cumd = g.edges(abs(g.superedges(id).edgeid(i))).cumd;
    
%     tpath = [tpath path];
    
    [~,inds] = max(and(bsxfun(@le, g.edges(abs(g.superedges(id).edgeid(i))).cumd(1:end-1), (thist.*lens(i))'), ...
        bsxfun(@ge, g.edges(abs(g.superedges(id).edgeid(i))).cumd(2:end), (thist.*lens(i))')),[],2);
    cums = (cumd(inds+1)-cumd(inds));
    cums(cums==0) = 1;
    p(:,interval==i) = path(:,inds) + (path(:,inds+1)-path(:,inds)).*...
        repmat((thist.*lens(i)-cumd(inds))./cums, size(g.nodes,2), 1);
end
% plot(tpath(1,:)', tpath(2,:)'); hold on; plot(p(1,:)', p(2,:)', 'bx')

end