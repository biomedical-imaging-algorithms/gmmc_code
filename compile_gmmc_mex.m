

%%%% CHANGE THESE PATHS IF DIFFERENT %%%%
% Path to Eigen header files
eigen_dir = '/usr/local/include/Eigen/';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

list_cpp = dir('mex/*.cpp');
full_path = [pwd '/mex/'];


command = ['mex -O -I' eigen_dir];
command = [command ' CXXFLAGS=''$CXXFLAGS -fPIC -O3 -DEIGEN_NO_DEBUG'''];
command = [command ' ' full_path 'mex_run_gmmc.cpp'];
command = [command ' ' full_path 'Graph.cpp'];
command = [command ' ' full_path 'MatrixOp.cpp'];
command = [command ' ' full_path 'MCT.cpp'];
command = [command ' ' full_path 'Superedege.cpp'];
command = [command ' ' full_path 'TransModel.cpp'];
command = [command ' ' full_path 'TreeSearch.cpp'];
command = [command ' ' full_path 'UniqueMv.cpp'];

eval(command);