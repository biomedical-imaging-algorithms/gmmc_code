function A = getSuperedgeDescriptors(A, ws)

% Get descriptors
for i=1:length(A.superedges)
    
    full_path = A.edges(abs(A.superedges(i).edgeid(1))).path;
    if A.superedges(i).edgeid(1)<0, full_path = fliplr(full_path); end;
    for k=2:length(A.superedges(i).edgeid)
        path = A.edges(abs(A.superedges(i).edgeid(k))).path;
        if A.superedges(i).edgeid(k)<0, path = fliplr(path); end;
        full_path = [full_path path(:,2:end)]; %#ok<AGROW>
    end
    dists = sqrt(sum(bsxfun(@minus, full_path(:,1), full_path).^2,1));
    dists = dists./dists(end);
    
    [~,indices] = max((repmat(dists(1,1:end-1),size(ws,2),1,size(ws,1)) <= repmat(permute(ws', [1 3 2]), 1, length(dists)-1)) .* ...
        (repmat(dists(1,2:end),size(ws,2),1,size(ws,1)) >= repmat(permute(ws', [1 3 2]), 1, length(dists)-1)),[],2);
    inds = reshape(indices, size(ws,2), size(ws,1));
    
    diffs = dists(inds+1)-dists(inds);
    diffs(diffs==0) = 1;
    
    sampling_points = (ws'-dists(inds))./diffs;
    
    points = full_path(:,inds) + (full_path(:,inds+1)-full_path(:,inds)) .* repmat(sampling_points(:)', size(A.nodes,2), 1);
    points = reshape(points, size(points,1), size(ws,2), size(ws,1));
    
    A.superedges(i).desc = permute(sum(sqrt(sum(diff(points,[],2).^2,1)),2), [1 3 2]);
end


% Order superedges by default order
Acell = [permute(struct2cell(A.superedges),[3 1 2]) num2cell(cellfun(@(x) length(x), {A.superedges.edgeid})')];
Acell = [sortrows(Acell(cellfun(@(x) x(1)~=0, Acell(:,2)),:), [5 -3]); sortrows(Acell(cellfun(@(x) x(1)==0, Acell(:,2)),:), -3)]; % sort by edgeid size and only then length and virtual edges
% Acell = [sortrows(Acell(cellfun(@(x) x(1)~=0, Acell(:,2)),:), -3); sortrows(Acell(cellfun(@(x) x(1)==0, Acell(:,2)),:), -3)]; % sort only by length
Acell = permute(Acell(:,1:4), [2 3 1]);

A.superedges = cell2struct(Acell, fieldnames(A.superedges), 1);




end





