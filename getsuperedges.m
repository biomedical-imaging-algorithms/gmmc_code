function g = getsuperedges(g,options)
% graph = getsuperedges(graph, params)
% 
% Calculates the superedges of a given graph
% 
% graph  - graph struct
% params - parameters for the GMMC method
%        params.K   - max number of edges in each superedge
%        params.d_c - max Euclidean distance for virtual superedges

if exist('graphshortestpath','file')
    [hm,paths] = hopmatrix(g.dA);
else
    [hm,paths] = hopmatrix_old(g.dA);
end
g.hm = hm;
g.sdA = sparse(zeros(size(g.dA)));

spe = [];
spedges = [];
for i=1:size(hm,1)
    for j=1:size(hm,2)
%         fprintf('%d %d\n', i, j);
        if i==j, continue; end;
        if hm(i,j)<=options.K
            spedges(end+1).path = []; %#ok<AGROW>
            spedges(end).edgeid = [];
            spedges(end).length = 0;
            spe.path = paths{i,j};
            g.sdA(paths{i,j}(1),paths{i,j}(end)) = length(spedges);
            spe.edgeid = [];
            spe.length = 0;
            for k=1:length(spe.path)-1
                for l=1:length(g.edges)
                    if all(spe.path([k k+1])==g.edges(l).endpoints)
                        spe.edgeid = [spe.edgeid l];
                        spe.length = spe.length + g.edges(l).length;
                        break;
                    end
                    if all(spe.path([k+1 k])==g.edges(l).endpoints)
                        spe.edgeid = [spe.edgeid -l];
                        spe.length = spe.length + g.edges(l).length;
                        break;
                    end
                end
            end
            spedges(end) = spe;
        end
    end
    progressbar(i,size(hm,1),20);
end
g.superedges = spedges;

% if options.directional==0
%     g.sdA = g.sdA+g.sdA';
% end

end