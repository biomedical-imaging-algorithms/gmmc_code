% Script requires run_gmmc_mex to be run before

dataset = 'retina_vutbr01';
fprintf('Fine aligning dataset %s\n', dataset);

thetas = [1 1 .1 .1];
betainv = .1;
THRESHOLD_MAH = .2;
THRESHOLD_EUCL = .005;
THRESHOLD_GEO = .05;
max_it = 10;

plotit=0;

A.points = [graphXr.X graphXr.Y];
B.points = [graphYr.X graphYr.Y];
if size(A.nodes,2)==3
    A.points = [A.points graphXr.Z];
    B.points = [B.points graphYr.Z];
end

switch dataset
    case 'retina_vutbr01'
        thetas = [2 5 .1 .8];
        betainv = .1;
        THRESHOLD_MAH = .1;
        THRESHOLD_EUCL = .0005;
        THRESHOLD_GEO = .05;
        max_it = 10;
end

if exist('geoDistX','var')
    A.gdm = geoDistX;
    B.gdm = geoDistY;
else
    A.gdm = graph_geodist(graphXr);
    B.gdm = graph_geodist(graphYr);
end

if ~exist('pathX','var')
    Dx = (graphXr.dA+graphXr.dA').*pdist2(A.points, A.points);
    pathX(size(A.nodes,1), size(A.nodes,1)).nodes = {};
    if size(A.nodes,2)==2
        [~, A.nodeidx] = min(pdist2([graphXr.X graphXr.Y], A.nodes));
    else
        [~, A.nodeidx] = min(pdist2([graphXr.X graphXr.Y graphXr.Z], A.nodes));
    end
    for i=1:size(A.nodes,1)
        [~, nodes] = graphshortestpath(Dx, A.nodeidx(i), 'Directed', false);
        for j=1:size(A.nodes,1)
            pathX(i,j).nodes = nodes{A.nodeidx(j)};
        end
    end
    Dy = (graphYr.dA+graphYr.dA').*pdist2(B.points, B.points);
    pathY(size(B.nodes,1), size(B.nodes,1)).nodes = {};
    if size(A.nodes,2)==2
        [~, B.nodeidx] = min(pdist2([graphYr.X graphYr.Y], B.nodes));
    else
        [~, B.nodeidx] = min(pdist2([graphYr.X graphYr.Y graphYr.Z], B.nodes));
    end
    for i=1:size(B.nodes,1)
        [~, nodes] = graphshortestpath(Dy, B.nodeidx(i), 'Directed', false);
        for j=1:size(B.nodes,1)
            pathY(i,j).nodes = nodes{B.nodeidx(j)};
        end
    end
end

R.nodeidx = A.nodeidx;
[R.points,cov2] = mygp(A.nodes(Mv(:,1),:), B.nodes(Mv(:,2),:), A.points, thetas, betainv);
Xpb = mygp(A.nodes(Mv(:,1),:), B.nodes(Mv(:,2),:), A.nodes, thetas, betainv);
cov2m = repmat(permute(cov2,[2 3 1]),size(A.nodes,2),size(A.nodes,2)).*repmat(eye(size(A.nodes,2)),[1,1,length(cov2)]);

mct_res = R.points;
bcorr = false;
if ~exist('Compl_Corr','var')
    bcorr=true;
end

if bcorr
    fprintf('Iteration 0 - %.4f\n', mean(sqrt(sum((Xpb(BCorr(:,1),:)-B.nodes(BCorr(:,2),:)).^2,2))));
else
    fprintf('Iteration 0 - %.4f\n', mean(sqrt(sum((R.points(Compl_Corr(:,1),:)-B.points(Compl_Corr(:,2),:)).^2,2))));
end

old_assignment = zeros(size(mct_res,1),1);
for it = 1:max_it
    
    candidates = find_candidates_set_graph(A.points, B.points, R.points, ...
        A.gdm, B.gdm, cov2m, ...
        A.nodeidx(Mv(:,1)), B.nodeidx(Mv(:,2)), ...
        THRESHOLD_MAH, THRESHOLD_EUCL, THRESHOLD_GEO, 1:size(A.points,1));
    
    R.nodes = R.points(R.nodeidx,:);
    
    Dmxbpybp = pdist2(R.nodes, B.nodes);
    Dmxbpybp(Dmxbpybp>sqrt(THRESHOLD_EUCL)) = Inf;
    [bp_assignment, bp_cost] = assignmentsuboptimal1(Dmxbpybp);
    num_bp_inliers = sum(bp_assignment>0);
    bp_inliers = find(bp_assignment);
    sel_bp_ass = bp_assignment ~= 0;
    
    xnodes = A.nodeidx(bp_inliers);
    ynodes = B.nodeidx(bp_assignment(bp_inliers));
    thispathX = pathX(bp_inliers,bp_inliers);
    thispathY = pathY(bp_assignment(bp_inliers),bp_assignment(bp_inliers));
    
    for i1 = 1:length(xnodes)
        for i2 = i1+1:length(xnodes)
            for idx_xedge = 1:length(thispathX((i1),(i2)).nodes)
                cand = candidates(thispathX((i1),(i2)).nodes(idx_xedge)).idx_points_close;
                rm_list = false(1,length(cand));
                for idx_cand = 1:length(cand)
                    iscand = find(cand(idx_cand) ==  thispathY((i1),(i2)).nodes);
                    if isempty(iscand)
                        rm_list(idx_cand) = true;
                    end
                end
                candidates(thispathX((i1),(i2)).nodes(idx_xedge)).idx_points_close(rm_list) = [];
                candidates(thispathX((i1),(i2)).nodes(idx_xedge)).dist_mah(rm_list) = [];
            end
        end
    end
    inds = logical(triu(ones(size(thispathY)))-eye(size(thispathY)));
    inner_ynodes = unique([thispathY( inds(:) ).nodes]);
    inds = logical(triu(ones(size(thispathX)))-eye(size(thispathX)));
    inner_xnodes = unique([thispathX( inds(:) ).nodes]);
    
    for idx_candidates = 1:length(candidates)
        if sum(idx_candidates == inner_xnodes)==0 %isempty(find(idx_candidates == inner_xnodes))
            cand = candidates(idx_candidates).idx_points_close;
            rm_list = false(1,length(cand));
            for idx_cand = 1:length(cand)
                iscand = find(cand(idx_cand) ==  inner_ynodes);
                if ~isempty(iscand)
                    rm_list(idx_cand) = true;
                end
            end
            candidates(idx_candidates).idx_points_close(rm_list) = [];
            candidates(idx_candidates).dist_mah(rm_list) = [];
        else
            cand = candidates(idx_candidates).idx_points_close;
            rm_list = false(1,length(cand));
            for idx_cand = 1:length(cand)
                iscand = find(cand(idx_cand) ==  inner_ynodes);
                if isempty(iscand)
                    rm_list(idx_cand) = true;
                end
            end
            candidates(idx_candidates).idx_points_close(rm_list) = [];
            candidates(idx_candidates).dist_mah(rm_list) = [];
        end
    end
    
    Dxy = Inf(size(A.points,1),size(B.points,1));
    for idx = 1:size(A.points,1)
        if ~isempty(candidates(idx).idx_points_close)
            Dxy(idx,candidates(idx).idx_points_close) = candidates(idx).dist_mah;
        end
    end
    [assignment, ~] = assignmentsuboptimal1(Dxy);
    
    if all(old_assignment==assignment)
        if bcorr
            fprintf('Iteration %d - %.4f\n', it, mean(sqrt(sum((Xpb(BCorr(:,1),:)-B.nodes(BCorr(:,2),:)).^2,2))));
        else
            fprintf('Iteration %d - %.4f\n', it, mean(sqrt(sum((R.points(Compl_Corr(:,1),:)-B.points(Compl_Corr(:,2),:)).^2,2))));
        end
        fprintf('Convergence in assignment reached\n');
        break;
    end
    old_assignment = assignment;
    
    sel_ass = assignment ~= 0;

    % remove some outliers (according to euclidean distance)
    cost = diag(Dxy(sel_ass, assignment(sel_ass)));
    cost(cost==10) = 0;
    euclidean_cost = sqrt( sum( (R.points(sel_ass,:) - B.points(assignment(sel_ass),:)).^2 , 2));
    [~,cost_idx] = sort(euclidean_cost,'descend');
    rm_idx = cost_idx(1:floor(size(cost,1)*0.05));
    sel_rm = find(sel_ass);
    sel_ass(sel_rm(rm_idx)) = 0;

    x = mct_res(sel_ass,:);
    y = B.points(assignment(sel_ass),:);
    
    [R.points,cov2] = mygp(x, y, mct_res, thetas, betainv);
    cov2m = repmat(permute(cov2,[2 3 1]),size(A.nodes,2),size(A.nodes,2)).*repmat(eye(size(A.nodes,2)),[1,1,length(cov2)]);
    
    if plotit
        h1=figure(1); cla %#ok<*UNRCH>
        hold on;
        plot(A.points(:,1), A.points(:,2),'c.','MarkerSize',8,'LineWidth',2)
%         plot(mct_res(:,1), mct_res(:,2),'c.','MarkerSize',8,'LineWidth',2);
        plot(R.points(:,1), R.points(:,2),'m.','MarkerSize',8,'LineWidth',2);
        plot(B.points(:,1), B.points(:,2),'r.','MarkerSize',8,'LineWidth',2)
        for i=1:size(x,1), line([x(i,1) y(i,1)],[x(i,2) y(i,2)],'LineWidth',2,'Color',[0 1 0]); end
    end
    if bcorr
        fprintf('Iteration %d - %.4f\n', it, mean(sqrt(sum((Xpb(BCorr(:,1),:)-B.nodes(BCorr(:,2),:)).^2,2))));
    else
        fprintf('Iteration %d - %.4f\n', it, mean(sqrt(sum((R.points(Compl_Corr(:,1),:)-B.points(Compl_Corr(:,2),:)).^2,2))));
    end
end

if it==max_it
    fprintf('Maximum number of iterations reached\n');
end

% fprintf('Elapsed time: %.4f s\n', t_mcts_fineAlign);

clf;
plot_pc(R.points, B.points);

save([res_path dataset '_fine.mat'], 'A', 'B', 'mct_res', 'R');




