//
//  Superedege.cpp
//  mcts
//
//  Created by Miguel Amável Pinheiro on 17/6/14.
//

#include "Superedege.h"

bool issecomp(VectorXd desc1, VectorXd desc2, double epsilon)
{
    for (int i=0; i<desc1.size(); i++)
        if ( 1/(1+epsilon) * desc1(i) > desc2(i) ) return false;
    
    for (int i=0; i<desc1.size(); i++)
        if ( desc2(i) > (1+epsilon) * desc1(i) ) return false;

    return true;
}