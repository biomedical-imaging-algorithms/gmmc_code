//
//  TransModel.h
//  mcts
//
//  Created by Miguel Amável Pinheiro on 14/12/14.
//

#ifndef __mcts__TransModel__
#define __mcts__TransModel__

#define IS_TEST 0
#define SELECTION_DEBUG 0
#define TIME_EXPANSION_EVAL 0
#define TIME_SIMULATION_EVAL 0
#define TIME_COMPUTENODE_EVAL 0
#define MODEL_CHECK_DEBUG 0
#define OUTPUT_TIMES 0

#include <iostream>
#include <iomanip>
#include <Dense>
#include <QR>
#include "Graph.h"
#include "MCT.h"
#include "MatrixOp.h"

/**
  Set of functions which do operations on the assumed transformation model 
*/

void removeModelIncompatible(Node &node, Graph &graphA, Graph &graphB, Params &params);
bool checkModelCompatibility(Node &node, VectorXd X, VectorXd Y, Graph &graphA, Graph &graphB, Params &params);

void updateModel(Node &node, Graph &graphA, Graph &graphB, Params &params);
//Params initTransfModel(Params params, int dim);
MatrixXd beta3(MatrixXd input);
MatrixXd findR3(MatrixXd &X, MatrixXd &Y);
MatrixXd findR2(MatrixXd X, MatrixXd Y);
MatrixXd applyRigidTransformation(MatrixXd X, MatrixXd T);
//MatrixXd controlPoints(MatrixXd grid_size, int rows);
void fit_rigbspline(MatrixXd &X, MatrixXd &Y, MatrixXd &ctrl, MatrixXd& Tr, MatrixXd& mu);

MatrixXd projectPoints(MatrixXd pts2Project, MatrixXd ptsA, MatrixXd ptsB, Params params);

//Test
void kalmanup_vs_icip(MatrixXd ptsA, MatrixXd ptsB);

#endif /* defined(__mcts__TransModel__) */
