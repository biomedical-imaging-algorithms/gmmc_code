//
//  MCT.h
//  mcts
//
//  Created by Miguel Amável Pinheiro on 24/6/14.
//

#ifndef __mcts__MCT__
#define __mcts__MCT__

#include <iostream>
#include <vector>
#include <Dense>
#include <sys/time.h>

using namespace std;
using namespace Eigen;

struct BsplModel
{
    MatrixXd Ct;
    vector<MatrixXd> Pt; //vector has as many elements as dimensions of the graphs
//    MatrixXd PtI;
};

/**
  The Node struct represents a node in the search tree
*/
struct Node
{
    MatrixXi Mv; // matched nodes
    MatrixXi Ms; // matched superedges
    double Q; // Reward Q
    double Qplus; // Reward Q+
    double Qtilde; // Reward Q~
    
    MatrixXi possiblept; // list of possible extendable nodes
    MatrixXi possiblese; // list of possible extendable superedges
    VectorXi seexpanded; // 0 if not expanded, 1 otherwise
    
    VectorXi priority; // priority of extendable superedges
    VectorXi skippeda; // list of skipped nodes in A
    VectorXi skippedb; // list of skipped nodes in B
    int visited; // number of times node and its children have been visited
    int notUnique; // -1 if unique, index of unique node if >=0
    
    vector<int> children; // node's children
    int index; // index in the monte carlo search tree
    bool not_expandable; // flag 1 if all nodes in subtree below are fully expanded, 0 otherwise
    
    BsplModel trModel;
    
    double time_elapsed;
};

/**
  The MCST struct represents the search tree used in the GMMC algorithm
*/
struct MCST
{
    vector<Node> nodes;
    vector<int> parent;
};

MCST subtree(MCST tree, int index, VectorXi &indxs);
double gettimediff(struct timeval end, struct timeval begin);

#endif /* defined(__mcts__MCT__) */
