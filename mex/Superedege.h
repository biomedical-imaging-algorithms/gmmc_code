//
//  Superedege.h
//  mcts
//
//  Created by Miguel Amável Pinheiro on 17/6/14.
//

#ifndef __mcts__Superedege__
#define __mcts__Superedege__

#include <iostream>
#include <Dense>

using namespace Eigen;

/**
 The Edge struct represents a geometrical edge
    endpoints:  1 x 2 array with indices of respective endpoint vertices
    path:       D x m matrix with path of the edge
    cumd:       1 x m array with cumulative distance of the path
    length:     total length of the path
*/
struct Edge
{
    Eigen::VectorXi endpoints;
    Eigen::MatrixXd path;
    Eigen::VectorXd cumd;
    double length;
};

/**
 The Superedge struct represents a geometrical superedge or set of consecutive edges
    path:       1 x s+1 array with indices of the vertices through which the superedge has its path
    edgeid:     1 x s array with indices of the respective edges of the superedge
    length:     total length of the superedge
    desc:       1 x |Omega| path descriptor
*/
struct Superedge
{
    VectorXi path;
    VectorXi edgeid;
    double length;
    VectorXd desc;
};

bool issecomp(VectorXd desc1, VectorXd desc2, double epsilon);

MatrixXd zeta_superedge(Superedge sed, VectorXd t);

#endif /* defined(__mcts__Superedege__) */
