//
//  FineAlign.cpp
//  mcts
//
//  Created by Miguel Amável Pinheiro on 3/1/15.
//

#include "FineAlign.h"

MatrixXd fineAlign(Graph graphA, Graph graphB, MatrixXd ptsA, MatrixXd ptsB, MatrixXi Mv, MatrixXi Compl_Corr, Params params)
{
    MatrixXd nodesR, ptsR;
    MatrixXd X(Mv.cols(), graphA.D), Y(Mv.cols(), graphA.D);
    int i, it;
    
    cout << "Fine alignment" << endl;
    
    MatrixXd Xcorr(Compl_Corr.rows(), graphA.D), Ycorr(Compl_Corr.rows(), graphA.D);
    
    if (graphA.D==2)
    {
        removeColXd(ptsA,2);
        removeColXd(ptsB,2);
    }
    
    for(i=0; i<Compl_Corr.rows(); i++)
        Ycorr.row(i) = ptsB.row(Compl_Corr(i,1)-1);
    
    //Project points based on coarse correspondence found from MCTS
    for(int m=0; m<Mv.cols(); m++)
    {
        X.row(m) = graphA.nodes.row(Mv(0,m));
        Y.row(m) = graphB.nodes.row(Mv(1,m));
    }
    nodesR = projectPoints(graphA.nodes, X, Y, params);
    ptsR = projectPoints(ptsA, X, Y, params);
    for(i=0; i<Compl_Corr.rows(); i++)
        Xcorr.row(i) = ptsR.row(Compl_Corr(i,0)-1);
    
    cout << "Iteration 0: " << ((Ycorr-Xcorr).array() * (Ycorr-Xcorr).array()).rowwise().sum().array().sqrt().mean() << endl;
    char filename[128], outFilename[128];
    strcpy(filename, "/Users/amavel/Documents/treematching/methods/mcts_cpp/treeXr.swc");
    strcpy(outFilename, "/Users/amavel/Documents/treematching/methods/mcts_cpp/treeZr0.swc");
    copySwcWritePoints(filename, outFilename, ptsR);
    
    MatrixXd Dxy = getDistance(nodesR, graphB.nodes);
    Assignment assign = assignmentsuboptimal(Dxy);
    
    VectorXi ass = assign.ass, ass_ant = VectorXi::Zero(assign.ass.size());
    
    //While convergence criteria are not met loop
    it=0;
    while( !(ass.array()==ass_ant.array()).all() && it<params.N_fine )
    {
        X = MatrixXd((ass.array()!=-1).count(), X.cols());
        Y = MatrixXd((ass.array()!=-1).count(), Y.cols());
        
        i=0;
        for(int m=0; m<assign.ass.size(); m++)
        {
            if(assign.ass(m)!=-1)
            {
                X.row(i) = graphA.nodes.row(m);
                Y.row(i) = graphB.nodes.row(assign.ass(m));
                i++;
            }
        }
        nodesR = projectPoints(graphA.nodes, X, Y, params);
        ptsR = projectPoints(ptsA, X, Y, params);
        ass_ant = ass;
        
        Dxy = getDistance(nodesR, graphB.nodes);
        assign = assignmentsuboptimal(Dxy);
        ass = assign.ass;
        
        it++;
        
        for(i=0; i<Compl_Corr.rows(); i++)
            Xcorr.row(i) = ptsR.row(Compl_Corr(i,0)-1);
        
        cout << "Iteration " << it << ": " << ((Ycorr-Xcorr).array() * (Ycorr-Xcorr).array()).rowwise().sum().array().sqrt().mean() << endl;
        sprintf(outFilename, "/Users/amavel/Documents/treematching/methods/mcts_cpp/treeZr%d.swc", it);
        copySwcWritePoints(filename, outFilename, ptsR);
    }
    
    return ptsR;
}






