//
//  TransModel.cpp
//  mcts
//
//  Created by Miguel Amável Pinheiro on 14/12/14.
//

#include "TransModel.h"

//Params initTransfModel(Params params, int dim)
//{
//    MatrixXd grs(dim,2);
//    for (int i=0; i<dim; i++)
//    {
//        grs(i,0) = -2;
//        grs(i,1) = 2;
//    }
////    params.ctrl = controlPoints(grs, 5);
//    
//    return params;
//}

MatrixXd beta3(MatrixXd input)
{
    return (abs(input.array())>1 && abs(input.array())<=2).select(MatrixXd::Ones(input.rows(),input.cols()), 0).array() * (2 - abs(input.array())).array().cube()*(1.0/6.0)
                 + (abs(input.array())<=1).select(MatrixXd::Ones(input.rows(),input.cols()), 0).array() * ( -abs(input.array()).array().square() + .5*abs(input.array()).array().cube() + (2.0/3.0) );
}

MatrixXd blkdiag(const MatrixXd& a, int count)
{
    MatrixXd bdm = MatrixXd::Zero(a.rows() * count, count);
    for (int i = 0; i < count; ++i)
    {
        bdm.block(i * a.rows(), i, a.rows(), 1) = a.col(i);
    }

    return bdm;
}

MatrixXd blkdiagrep(const MatrixXd& a, int count)
{
    MatrixXd bdm = MatrixXd::Zero(a.rows() * count, a.cols() * count);
    for (int i = 0; i < count; ++i)
    {
        bdm.block(i * a.rows(), i * a.cols(), a.rows(), a.cols()) = a;
    }

    return bdm;
}

MatrixXd findR3(MatrixXd &X, MatrixXd &Y)
{
    MatrixXd XX(X.rows(), X.cols()+1), YY(Y.rows(), Y.cols()+1);
    XX.block(0, 0, X.rows(), X.cols()) = X;
    XX.block(0, X.cols(), X.rows(), 1) = MatrixXd::Ones(X.rows(), 1);
    YY.block(0, 0, Y.rows(), Y.cols()) = Y;
    YY.block(0, Y.cols(), Y.rows(), 1) = MatrixXd::Ones(Y.rows(), 1);
    
    return XX.colPivHouseholderQr().solve(YY);
}

MatrixXd findR2(MatrixXd X, MatrixXd Y)
{
    MatrixXd Xm = X.colwise().mean();
    MatrixXd Ym = Y.colwise().mean();
    
    MatrixXd Xd = (X - Xm.replicate(X.rows(), 1)).transpose()*(Y - Ym.replicate(Y.rows(), 1));
    
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(Xd, Eigen::ComputeFullU | Eigen::ComputeFullV);
    MatrixXd R = svd.matrixU()*svd.matrixV().transpose();
    MatrixXd T = Ym - Xm*R;
    
    MatrixXd Tr(R.rows()+1, R.rows()+1);
    Tr << R, MatrixXd::Zero(R.rows(), 1), T, 1;
    
    return Tr;
}

MatrixXd findR(MatrixXd &X, MatrixXd &Y)
{
    VectorXd T = (Y-X).colwise().mean();
    MatrixXd Xt = X.rowwise() + T.transpose();
    
    MatrixXd Xd = Xt.transpose()*Y;
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(Xd, Eigen::ComputeFullU | Eigen::ComputeFullV);
    MatrixXd R = svd.matrixU()*svd.matrixV().transpose();
    
    MatrixXd Tr(R.rows()+1, R.rows()+1);
    Tr << R, MatrixXd::Zero(R.rows(), 1), T.transpose(), 1;
    
    return Tr;
}

MatrixXd applyRigidTransformation(MatrixXd X, MatrixXd T)
{
    MatrixXd newX(X.rows(),X.cols()+1);
    newX.block(0, 0, X.rows(), X.cols()) = X;
    newX.block(0, X.cols(), X.rows(), 1) = MatrixXd::Ones(X.rows(), 1);
    MatrixXd res = newX*T;
    res.conservativeResize(X.rows(), X.cols());
    
    return res;
}

void fit_rigbspline(MatrixXd &X, MatrixXd &Y, MatrixXd &ctrl, MatrixXd& Tr, MatrixXd& mu)
{
    Tr = findR3(X, Y);
    MatrixXd Xrig = applyRigidTransformation(X, Tr);
    
    MatrixXd Beta = beta3(Xrig.col(0).replicate(1, ctrl.rows()) - ctrl.col(0).transpose().replicate(Xrig.rows(), 1));
    for (int i=1; i<Xrig.cols(); i++) {
        Beta = Beta.array() * beta3((Xrig.col(i).replicate(1, ctrl.rows()) - ctrl.col(i).transpose().replicate(Xrig.rows(), 1))).array();
    }
    MatrixXd A(Beta.rows()+Beta.cols(), Beta.cols()), b(Xrig.rows()+Beta.cols(),Xrig.cols());
    A.block(0, 0, Beta.rows(), Beta.cols()) = Beta;
    A.block(Beta.rows(), 0, Beta.cols(), Beta.cols()) = MatrixXd::Identity(Beta.cols(), Beta.cols());
    b.block(0, 0, Beta.rows(), Y.cols()) = Y-Xrig;
    b.block(Beta.rows(), 0, Beta.cols(), Y.cols()) = MatrixXd::Zero(Beta.cols(), Y.cols());
    
//    MatrixXd A = Beta, b = Y-Xrig;
    
//    cout << A.rows() << "," << A.cols() << " " << Beta.rows() << "," << Beta.cols() << " " << b.rows() << "," << b.cols() << endl;
//                clock_t begin = clock();
    mu = A.colPivHouseholderQr().solve(b);
//                clock_t end = clock();
//                cout << gettimediff(end, begin) << "s" << endl;
}

MatrixXd phi(MatrixXd xt, MatrixXd ctrl)
{
    MatrixXd phi = beta3(xt.col(0).replicate(1, ctrl.rows()) - ctrl.col(0).transpose().replicate(xt.rows(), 1));
    for (int i=1; i<xt.cols(); i++) {
        phi = phi.array() * beta3((xt.col(i).replicate(1, ctrl.rows()) - ctrl.col(i).transpose().replicate(xt.rows(), 1))).array();
    }
    return phi.transpose();
}

void updateModel(Node &node, Graph &graphA, Graph &graphB, Params &params)
{
    VectorXd xt, yt, tilde_yt, phi_t;
    MatrixXd identity = MatrixXd::Identity(params.ctrl.rows(), params.ctrl.rows());
    struct timeval begin, end;
    
    xt = graphA.nodes.row(node.Mv(0,node.Mv.cols()-1)), yt = graphB.nodes.row(node.Mv(1,node.Mv.cols()-1));
    phi_t = phi(xt.transpose(), params.ctrl);
    tilde_yt = yt.transpose() - (xt.transpose() + phi_t.transpose() * node.trModel.Ct);
    
    if (params.noise_dim_independent)
    {
        VectorXd kt(params.ctrl.rows());
        double st;
        
        st = (phi_t.transpose() * node.trModel.Pt[0] * phi_t + params.sigma_r(0));
        kt = node.trModel.Pt[0] * phi_t / st;
        node.trModel.Ct = node.trModel.Ct + kt * tilde_yt.transpose();
        
//        gettimeofday(&begin, NULL);
        node.trModel.Pt[0] = (identity - kt * phi_t.transpose()) * node.trModel.Pt[0];
//        gettimeofday(&end, NULL); cout << " " << gettimediff(end, begin) << endl;
    }
    else
    {
        MatrixXd kt(params.ctrl.rows(), graphA.D);
        double st;
    //    MatrixXd ud, sd, vd, Pt;
    //    VectorXd diagP;
    //    int k = 5;
        
        for (int d=0; d<graphA.D; d++)
        {
            
            
            st = (phi_t.transpose() * node.trModel.Pt[d] * phi_t + params.sigma_r(d));
            kt.col(d) = node.trModel.Pt[d] * phi_t / st;
            node.trModel.Ct.col(d) = node.trModel.Ct.col(d) + kt.col(d) * tilde_yt(d);
            gettimeofday(&begin, NULL);
            node.trModel.Pt[d] = (identity - kt.col(d) * phi_t.transpose()) * node.trModel.Pt[d];
            gettimeofday(&end, NULL); printf(" %f\n", gettimediff(end, begin));
            
    //        begin = clock();
    //        diagP = node.trModel.Pt[d].diagonal();
    //        Eigen::JacobiSVD<Eigen::MatrixXd> svd(node.trModel.Pt[d] - MatrixXd(diagP.asDiagonal()), Eigen::ComputeFullU | Eigen::ComputeFullV);
    //        end = clock(); cout << " " << double(end-begin)/CLOCKS_PER_SEC << endl;
    //        
    //        begin = clock();
    //        Pt = (identity - kt.col(d) * phi_t.transpose()) * svd.matrixU().block(0, 0, params.ctrl.rows(), k) * MatrixXd(svd.singularValues().asDiagonal()).block(0, 0, k, k) * svd.matrixV().block(0, 0, params.ctrl.rows(), k).transpose();
    //        end = clock(); cout << " " << double(end-begin)/CLOCKS_PER_SEC << endl;
        }
    }
    
//    begin = clock();
//    printToMatlab("ktp", blkdiag(kt, graphA.D) * blkdiagrep(phi_t.transpose(),graphA.D));
//    node.trModel.PtI = (MatrixXd::Identity(params.ctrl.rows()*graphA.D, params.ctrl.rows()*graphA.D) - blkdiag(kt, graphA.D) * phi_t.transpose().replicate(graphA.D, graphA.D)) * node.trModel.PtI;
//    end = clock(); cout << " " << double(end-begin)/CLOCKS_PER_SEC << endl;
}

MatrixXd projectPoints(MatrixXd pts2Project, MatrixXd ptsA, MatrixXd ptsB, Params params)
{
    MatrixXd Tr, mu, Beta;
    fit_rigbspline(ptsA, ptsB, params.ctrl, Tr, mu);
    
    pts2Project = applyRigidTransformation(pts2Project, Tr);
    Beta = beta3(pts2Project.col(0).replicate(1, params.ctrl.rows()) - params.ctrl.col(0).transpose().replicate(pts2Project.rows(), 1));
    for (int i=1; i<ptsA.cols(); i++) {
        Beta = Beta.array() * beta3((pts2Project.col(i).replicate(1, params.ctrl.rows()) - params.ctrl.col(i).transpose().replicate(pts2Project.rows(), 1))).array();
    }
    pts2Project = pts2Project + Beta*mu;
    return pts2Project;
}

void removeModelIncompatible(Node &node, Graph &graphA, Graph &graphB, Params &params)
{
    MatrixXd X(node.possiblept.rows(),graphA.D), Y(node.possiblept.rows(),graphA.D);
    for (int i=0; i<node.possiblept.rows(); i++)
    {
        X.row(i) = graphA.nodes.row(node.possiblept(i,0));
        Y.row(i) = graphB.nodes.row(node.possiblept(i,1));
    }
    if (params.transfModel==1)
    {
        MatrixXd St = MatrixXd::Zero(graphA.D, graphA.D);
        MatrixXd phi_t = phi(X.transpose(), params.ctrl);
        
        for (int i=(int)X.rows()-1; i>=0; i--)
        {
            if (params.noise_dim_independent)
                St = MatrixXd::Identity(graphA.D, graphA.D).array() * (1 / (phi_t.col(0).transpose() * node.trModel.Pt[0] * phi_t.col(0) + params.sigma_r(0)));
            else
                for (int d=0; d<graphA.D; d++)
                    St(d,d) = 1 / (phi_t.col(i).transpose() * node.trModel.Pt[d] * phi_t.col(i) + params.sigma_r(d));
            
            double dist = (Y.row(i) - (X.row(i) + phi_t.col(i).transpose() * node.trModel.Ct)) * St * (Y.row(i) - (X.row(i) + phi_t.col(i).transpose() * node.trModel.Ct)).transpose();
            if (dist>=params.d_m) removePossibleSuperedge(node, i);
            
            #if IS_TEST
            printf("%f\n", dist);
            #endif
        }
    }
    else if (params.transfModel==0)
    {
        for (int i=(int)X.rows()-1; i>=0; i--)
        {
            for (int j=0; j<node.Mv.cols()-1; j++)
            {
                double distA = sqrt((graphA.nodes.row(node.Mv(0,j))-X.row(i))*(graphA.nodes.row(node.Mv(0,j))-X.row(i)).transpose());
                double distB = sqrt((graphB.nodes.row(node.Mv(1,j))-Y.row(i))*(graphB.nodes.row(node.Mv(1,j))-Y.row(i)).transpose());
                if (1/(1+params.epsilon_T) * distA > distB || distB > (1+params.epsilon_T) * distA)
                {
                    removePossibleSuperedge(node, i);
                    break;
                }
            }
        }
    }
}

bool checkModelCompatibility(Node &node, VectorXd X, VectorXd Y, Graph &graphA, Graph &graphB, Params &params)
{
    if (params.transfModel==1)
    {
        MatrixXd St = MatrixXd::Zero(graphA.D, graphA.D);
        MatrixXd phi_t = phi(X.transpose(), params.ctrl);
        
        if (params.noise_dim_independent)
            St = MatrixXd::Identity(graphA.D, graphA.D).array() * (1 / (phi_t.col(0).transpose() * node.trModel.Pt[0] * phi_t.col(0) + params.sigma_r(0)));
        else
            for (int d=0; d<graphA.D; d++)
                St(d,d) = 1 / (phi_t.col(0).transpose() * node.trModel.Pt[d] * phi_t.col(0) + params.sigma_r(d));
    
        double dist = (Y.transpose() - (X.transpose() + phi_t.col(0).transpose() * node.trModel.Ct)) * St * (Y.transpose() - (X.transpose() + phi_t.col(0).transpose() * node.trModel.Ct)).transpose();
        #if MODEL_CHECK_DEBUG
        printf("%f ", dist);
        #endif
    
        if (dist>=params.d_m) return false;
        else return true;
    }
    else if (params.transfModel==0)
    {
        for (int j=0; j<node.Mv.cols()-1; j++)
        {
            double distA = sqrt((graphA.nodes.row(node.Mv(0,j))-X.transpose())*(graphA.nodes.row(node.Mv(0,j))-X.transpose()).transpose());
            double distB = sqrt((graphB.nodes.row(node.Mv(1,j))-Y.transpose())*(graphB.nodes.row(node.Mv(1,j))-Y.transpose()).transpose());
            if (1/(1+params.epsilon_T) * distA > distB || distB > (1+params.epsilon_T) * distA)
            {
                #if MODEL_CHECK_DEBUG
                printf("0 ");
                #endif
                return false;
            }
        }
        #if MODEL_CHECK_DEBUG
        printf("1 ");
        #endif
        return true;
    }
    return false;
}

//For test of Kalman vs previous approach

MatrixXd controlPoints(MatrixXd grid_size, int rows)
{
    MatrixXd xm = RowVectorXd::LinSpaced(rows, grid_size(0,0), grid_size(0,1)).replicate(rows,1);
    MatrixXd ym = VectorXd::LinSpaced(rows, grid_size(1,0), grid_size(1,1)).replicate(1,rows);
    
    VectorXd x(Map<VectorXd>(xm.data(), xm.cols()*xm.rows()));
    VectorXd y(Map<VectorXd>(ym.data(), ym.cols()*ym.rows()));
    
    MatrixXd ctrl(x.size(),2); ctrl << x, y;
    
    if(grid_size.rows()==3)
    {
        VectorXd zm = VectorXd::LinSpaced(rows, grid_size(2,0), grid_size(2,1));
        MatrixXd newctrl(ctrl.rows()*zm.size(),3);
        for (int i=0; i<zm.size(); i++)
        {
            newctrl.block(i*ctrl.rows(), 0, ctrl.rows(), 2) = ctrl;
            newctrl.block(i*ctrl.rows(), 2, ctrl.rows(), 1) =  MatrixXd::Ones(ctrl.rows(), 1)*zm(i);
        }
        ctrl = newctrl;
    }
    
    return ctrl;
}

void kalmanup_vs_icip(MatrixXd X, MatrixXd Y)
{
    double P0_ini = 100.0, st;
    int dim=2;
    double sig_r = 0.01*0.01, d_m = 1.0;
    
    MatrixXd grs(dim,2), ctrl;
    for (int i=0; i<dim; i++)
    {
        grs(i,0) = -2;
        grs(i,1) = 2;
    }
    ctrl = controlPoints(grs, 5);
    
    MatrixXd tmp = X.block(0, 0, X.rows(), dim); X = tmp;
    tmp = Y.block(0, 0, Y.rows(), dim); Y = tmp;
    
    PermutationMatrix<Dynamic,Dynamic> perm((int)X.rows());
    perm.setIdentity();
    std::random_shuffle(perm.indices().data(), perm.indices().data()+perm.indices().size());
    
    X = perm*X;
    Y = perm*Y;
    
    tmp = X.replicate(20, 1); X = tmp;
    tmp = Y.replicate(20, 1); Y = tmp;
    
    MatrixXd x = X.block(0, 0, 10, dim);
    MatrixXd y = Y.block(0, 0, 10, dim);
    
    MatrixXd Tr = findR2(x,y);
    MatrixXd crig = applyRigidTransformation(ctrl, Tr);
    
    MatrixXd Beta = beta3(ctrl.col(0).replicate(1, ctrl.rows()) - ctrl.col(0).transpose().replicate(ctrl.rows(), 1));
    for (int i=1; i<ctrl.cols(); i++) {
        Beta = Beta.array() * beta3((ctrl.col(i).replicate(1, ctrl.rows()) - ctrl.col(i).transpose().replicate(ctrl.rows(), 1))).array();
    }
    MatrixXd mu = Beta.colPivHouseholderQr().solve(crig-ctrl);
    
    MatrixXd C0 = mu;
    vector<MatrixXd> P0;
    for (int i=0; i<dim; i++) {
        P0.push_back(Beta.array()*P0_ini);
    }
    
    MatrixXd Ctold = C0;
    vector<MatrixXd> Ptold = P0;
    MatrixXd kt(ctrl.rows(), dim);
    
    VectorXd kt_all_times = VectorXd::Zero(X.rows()-2);
    for (int c=2; c<X.rows(); c++)
    {
        struct timeval begin;
        gettimeofday(&begin, NULL);
        
        VectorXd xt = X.row(c);
        VectorXd yt = Y.row(c);
        
        VectorXd phi_xt = phi(xt.transpose(), ctrl);
        MatrixXd St = MatrixXd::Zero(dim, dim);
        for (int d=0; d<dim; d++)
            St(d,d) = 1 / (phi_xt.transpose() * Ptold[d] * phi_xt + sig_r);
        
        double dist = (yt.transpose() - (xt.transpose() + phi_xt.transpose() * Ctold)) * St * (yt.transpose() - (xt.transpose() + phi_xt.transpose() * Ctold)).transpose();
        if (dist>=d_m*d_m) printf("heyho\n");
        
        MatrixXd tilde_yt = yt.transpose() - (xt.transpose() + phi_xt.transpose() * Ctold);
        
        for (int d=0; d<dim; d++)
        {
            st = 1 / (phi_xt.transpose() * Ptold[d] * phi_xt + sig_r);
            kt.col(d) = Ptold[d] * phi_xt * st;
            Ctold.col(d) = Ctold.col(d) + kt.col(d) * tilde_yt(d);
            Ptold[d] = (MatrixXd::Identity(ctrl.rows(), ctrl.rows()) - kt.col(d) * phi_xt.transpose()) * Ptold[d];
        }
        
        struct timeval end;
        gettimeofday(&end, NULL);
        kt_all_times(c-2) = gettimediff(end, begin);
    }
    
    VectorXd ic_all_times = VectorXd::Zero(X.rows()-2);
    for (int c=2; c<X.rows(); c++)
    {
        struct timeval begin;
        gettimeofday(&begin, NULL);
        
        VectorXd xt = X.row(c);
        VectorXd yt = Y.row(c);
        
        MatrixXd Xc = X.block(0, 0, c-1, dim);
        MatrixXd Yc = Y.block(0, 0, c-1, dim);
        
        for (int j=0; j<Xc.rows(); j++)
        {
            double distA = sqrt((Xc.row(j)-xt.transpose())*(Xc.row(j)-xt.transpose()).transpose());
            double distB = sqrt((Yc.row(j)-yt.transpose())*(Yc.row(j)-yt.transpose()).transpose());
            if (1/(1+.09) * distA > distB || distB > (1+.09) * distA)
            {
                printf("heyho\n");
            }
        }
        
        struct timeval end;
        gettimeofday(&end, NULL);
        ic_all_times(c-2) = gettimediff(end, begin);
    }
    
    cout << "kupd = [";
    for (int j=0; j<kt_all_times.size(); j++) cout << setprecision(9) << " " << kt_all_times(j);
    cout << "];\n";
    cout << "icip = [";
    for (int j=0; j<ic_all_times.size(); j++) cout << setprecision(9) << " " << ic_all_times(j);
    cout << "];\n";
//    printToMatlab("kupd", kt_all_times.transpose());
//    printToMatlab("icip", ic_all_times.transpose());
}














