//
//  TreeSearch.h
//  mcts
//
//  Created by Miguel Amável Pinheiro on 18/6/14.
//

#ifndef __mcts__TreeSearch__
#define __mcts__TreeSearch__

#include <iostream>
#include "TransModel.h"
#include "MCT.h"
#include "UniqueMv.h"
#include <sys/time.h>
#include <iomanip>
#ifdef MATLAB_MEX_FILE
#include <mex.h>
#endif

/**
  Main functions of the GMMC tree search algorithm
*/

Node runTreeSearch(Graph A, Graph B, Params &params, char *outputFile, MatrixXi Compl_Corr = MatrixXi(0,0));

int select(MCST &mct, int &node_p, int &it, double &Qnorm, Params &params);
void expand(MCST &mct, int it, vector<int> &expanded, int &n, Graph &graphA, Graph &graphB, UniqueMs &MsList, double avgLen, double Qnorm, Params &params);
void simulate(MCST &mct, vector<int> expanded, vector<int> &simulated, int &n, Graph &graphA, Graph &graphB, UniqueMs &MsList, double avgLen, double Qnorm, Params &params);
void backpropagate(MCST &mct, int c);

//int jumpSuperedge(Node node, Graph graphA, Graph graphB, Params params);
void computeNode(Node &node, int mctSize, int s, int n, Graph &graphA, Graph &graphB, UniqueMs &MsList, double avgLen, double Qnorm, Params &params);
//Node computeJumpNode(MCST mct, Node node, int s, int n, Graph graphA, Graph graphB, UniqueMs &MsList, double avgLen, double Qnorm, Params params);

int selectSuperedge(Node &node, Graph &graphA, Graph &graphB, Params &params);
int selectRootSuperedge(Node &node, Graph &graphA, Graph &graphB, Params &params);
double calculateQtilde(Node &node, int it, double Qnorm, Params &params);
void calculateQ(Node &node, Graph &graphA, Graph &graphB, double avgLen, Params &params);

void writeResult(char *filename, Node node, double time_elapsed);

#endif /* defined(__mcts__TreeSearch__) */
