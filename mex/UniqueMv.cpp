//
//  UniqueMv.cpp
//  mcts
//
//  Created by Miguel Amável Pinheiro on 24/11/14.
//

#include "UniqueMv.h"



int UniqueMs::isUnique(MatrixXi Ms, int indexInTree)
{
    int i, j, MsCols = (int)Ms.cols();
    bool equal;
//    struct timeval begin, end;
//    quicksort(Ms, 0, (int)Ms.cols()-1);
    
//    gettimeofday(&begin, NULL);
    for (i=0; i<Matches.size(); i++)
    {
        if (Matches[i].cols()==MsCols)
        {
            equal = true;
            for (j=0; j<MsCols; j++)
            {
                if (Ms(0,j)!=Matches[i](0,j) || Ms(1,j)!=Matches[i](1,j))
                {
                    equal = false;
                    break;
                }
            }
            if (equal) return NodeIndex[i];
        }
    }
//    gettimeofday(&end, NULL); cout << " " << gettimediff(end, begin) << "s dup " << i << endl;
    
    Matches.push_back(Ms);
    NodeIndex.push_back(indexInTree);
//    end = clock(); cout << " " << double(end-begin)/CLOCKS_PER_SEC << endl;
    
    return -1;
}

int partitionqs(MatrixXi &Ms, int left, int right)
{
    int pivotIndex = right;
    int pivotValue = Ms(0,pivotIndex);
    int storeIndex = left;
    int tmp1, tmp2;
    
    for (int i=left; i<right; i++)
    {
        if (Ms(0,i)<pivotValue)
        {
            tmp1 = Ms(0,i); tmp2 = Ms(1,i);
            Ms(0,i) = Ms(0,storeIndex); Ms(1,i) = Ms(1,storeIndex);
            Ms(0,storeIndex) = tmp1; Ms(1,storeIndex) = tmp2;
            storeIndex++;
        }
    }
    tmp1 = Ms(0,right); tmp2 = Ms(1,right);
    Ms(0,right) = Ms(0,storeIndex); Ms(1,right) = Ms(1,storeIndex);
    Ms(0,storeIndex) = tmp1; Ms(1,storeIndex) = tmp2;
    
    return storeIndex;
}

void quicksort(MatrixXi &Ms, int i, int k)
{
    int p;
    if (i<k)
    {
        p = partitionqs(Ms, i, k);
        quicksort(Ms, i, p-1);
        quicksort(Ms, p+1, k);
    }
}









