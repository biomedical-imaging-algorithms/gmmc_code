//
//  main.cpp
//  mcts
//
//  Created by Miguel Amável Pinheiro on 16/6/14.
//

#include <iostream>
#include "TreeSearch.h"
#include "FineAlign.h"

using namespace Eigen;

int main(int argc, const char * argv[])
{
    char filename[128], outFilename[128];
    Params params;
    Graph graphA, graphB;
    Node bestNode;
    MatrixXi Compl_Corr;
    MatrixXd ptsA, ptsB, ptsR;

    if(argc==1)
    {
        strcpy(filename, "/Users/amavel/Documents/treematching/methods/mcts_cpp/params.txt");
        params = readParamFile(filename);
        
        strcpy(filename, "/Users/amavel/Documents/treematching/methods/mcts_cpp/A");
        graphA = readGraph(filename, params);
        strcpy(filename, "/Users/amavel/Documents/treematching/methods/mcts_cpp/B");
        graphB = readGraph(filename, params);
        
        strcpy(filename, "/Users/amavel/Documents/treematching/methods/mcts_cpp/result");
        bestNode = runTreeSearch(graphA, graphB, params, filename);
    }
    else if(argc==6)
    {
        strcpy(filename, argv[4]);
        params = readParamFile(filename);
        
        strcpy(filename, argv[3]);
        Compl_Corr = readCompl_Corr(filename);
        
        //Read graphs
        strcpy(filename, argv[1]);
        graphA = readGraph(filename, params);
        strcpy(filename, argv[2]);
        graphB = readGraph(filename, params);
        
        //MCTS
        strcpy(filename, argv[5]);
        bestNode = runTreeSearch(graphA, graphB, params, filename, Compl_Corr);
    }
    else if(argc==9)
    {
        strcpy(filename, argv[6]);
        params = readParamFile(filename);
        
        //Read graphs
        strcpy(filename, argv[1]);
        graphA = readGraph(filename, params);
        strcpy(filename, argv[2]);
        graphB = readGraph(filename, params);
        
        //MCTS
        strcpy(filename, argv[7]);
        bestNode = runTreeSearch(graphA, graphB, params, filename);
        
        //Read swcs
        strcpy(filename, argv[3]);
        ptsA = readPointsFromSwc(filename);
        strcpy(filename, argv[4]);
        ptsB = readPointsFromSwc(filename);
        
        //Read True Correspondence for debug
        strcpy(filename, argv[5]);
        Compl_Corr = readCompl_Corr(filename);
        
        //Fine alignment
        ptsR = fineAlign(graphA, graphB, ptsA, ptsB, bestNode.Mv, Compl_Corr, params);
        
        strcpy(filename, argv[3]);
        strcpy(outFilename, argv[8]);
        copySwcWritePoints(filename, outFilename, ptsR);
    }
    else if(argc==3)
    {
        strcpy(filename, argv[1]);
        ptsA = readPointsFromSwc(filename);
        strcpy(filename, argv[2]);
        ptsB = readPointsFromSwc(filename);
        
        kalmanup_vs_icip(ptsA, ptsB);
    }
    else
    {
        cout << "Number of input arguments invalid\n";
        cout << "Usage: ./mcts [file_graphA] [file_graphB] [file_swcA] [file_swcB] [file_Compl_Corr] [file_Ps] [file_params] [file_output]\n";
    }
    
    return 0;
}

