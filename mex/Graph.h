//
//  Graph.h
//  mcts
//
//  Created by Miguel Amável Pinheiro on 16/6/14.
//

#ifndef __mcts__Graph__
#define __mcts__Graph__

#include <iostream>
#include <fstream>
#include "Superedege.h"
#include <vector>
#include <sstream>
#include <algorithm>
#include <iterator>

using namespace std;
using namespace Eigen;

/**
  The Params struct is a collection of parameters for the GMMC method
*/
struct Params
{
    int K;              //Maximum size of superedge (number of edges)
    
    double epsilon_h;   //Allowed deformation for superedges
    double epsilon_T;   //Allowed deformation in transformation model
    double kappa;       //Weight on reward or objective function
    double gamma;       //Weight on urgency (for number of times node is visited)
    int N;              //Maximum number of iterations
    int N_exp;          //Number of children added in Expansion
    int N_sim;          //Number of nodes added in Simulation
    int N_fine;         //Number of iterations in fine alignment function (not implemented)
    int termDepth;      //Number of matches at which the method stops
    double timeLimit;   //Maximum time in seconds for the method to stop
    
    bool simulateAll;   //Simulate all expanded nodes or not

    int transfModel;    //0 - epsilon check, 1 - Bspline with Kalman update
    int modelCheck;     //0 - Check all when adding node, 1 - Check only when selecting child to expand
    int ordering; //0 - Default ordering, 1 - Random
    
    //B-spline transformation parameters
    MatrixXd ctrl;      //control points of B-spline transformation
    double P0_ini;      //constant multiplied element-wise with the initial covariance of the B-spline transformation coefficients
    int P_initialization; // which initialization: 0 - P0 = P0_ini * I; 1 - P0 = P0_ini * Beta; 2 - P0 = P0_ini * ones; 
    VectorXd sigma_r;   //standard deviation of Gaussian noise of the transformation
    double d_m;         // max mahalanobis distance for a point to be considered in model
    bool noise_dim_independent; // internal variable
    
    int verbose;
};

/**
  The Graph struct represents a geometrical graph
    nodes:      n x D matrix with vertices of the graph
    edges:      1 x |E| array of structures with the following format:
    fnodes:     n x 1 array with labels for each vertex
    superedges: 1 x |S| array of structures with the following format:
    D:          dimensionality of the graph
  Check each header file for further description on each custom struct
*/
struct Graph
{
    MatrixXd nodes;
    vector<Edge> edges;
    VectorXi fnodes;
    vector<Superedge> superedges;
    int D;
};

MatrixXd zeta_superedge(Graph &G, int supered, VectorXd t);

Params readParamFile(char *filename);
Graph readGraph(char *filename, Params params);
MatrixXd readPointsFromSwc(char *filename);
MatrixXi readPs(char *filename);
MatrixXi readCompl_Corr(char *filename);
void copySwcWritePoints(char *originalSwcName, char *outputFile, MatrixXd points);

MatrixXd controlPoints(MatrixXd grid_size, VectorXi nctrlpts);

#endif /* defined(__mcts__Graph__) */
