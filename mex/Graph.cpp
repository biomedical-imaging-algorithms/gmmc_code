//
//  Graph.cpp
//  mcts
//
//  Created by Miguel Amável Pinheiro on 16/6/14.
//

#include "Graph.h"

using namespace std;


MatrixXd zeta_superedge(Graph &G, int supered, VectorXd t)
{
    Superedge sed = G.superedges[supered];
    MatrixXd p = MatrixXd::Zero(G.nodes.cols(), t.size());
    int i, j, d;
    double cumm;
    
    if (sed.edgeid(0)==0)
    {
        for (d=0; d<G.nodes.cols(); d++)
            p.row(d) = VectorXd::LinSpaced(p.cols(), G.nodes(sed.path(0),d), G.nodes(sed.path(1),d));
        return p;
    }
    
    VectorXd thist;
    
    VectorXd cumsum = VectorXd::Zero(sed.edgeid.size()+1);
    VectorXd lengths(sed.edgeid.size());
    VectorXd diffs(cumsum.size()-1);
    for(i=0; i<sed.edgeid.size(); i++)
    {
        lengths(i) = G.edges[abs(sed.edgeid(i))-1].length;
        cumsum(i+1) = G.edges[abs(sed.edgeid(i))-1].length + cumsum.sum();
    }
    
    cumsum = cumsum/cumsum(cumsum.size()-1);
    
    for(i=0; i<diffs.size(); i++) {
        diffs(i) = cumsum(i+1)-cumsum(i);
        if (diffs(i)==0) diffs(i)=1;
    }
    
    VectorXi interval = VectorXi::Zero(t.size());
    for(i=0; i<cumsum.size()-1; i++)
        interval += (t.array() >= cumsum(i)).select(1, VectorXi::Zero(t.size()));
    
    VectorXd newt(t.size());
    for (i=0; i<newt.size(); i++)
        newt(i) = (t(i)-cumsum(interval(i)-1))/diffs(interval(i)-1);
    
    int cnt=0, pind=0;
    for(i=0; i<sed.edgeid.size(); i++)
    {
        thist = newt.segment( cnt, (interval.array()==i+1).count() );
        cnt += thist.size();
        
        if (sed.edgeid(i)<0) thist = VectorXd::Ones(thist.size())-thist;
        
        thist = thist * lengths(i);
        
        for (j=0; j<thist.size(); j++)
        {
            long ind;
            (G.edges[abs(sed.edgeid[i])-1].cumd.segment(0, G.edges[abs(sed.edgeid[i])-1].cumd.size()-1).array() <= thist(j) &&
                G.edges[abs(sed.edgeid[i])-1].cumd.segment(1, G.edges[abs(sed.edgeid[i])-1].cumd.size()-1).array() >= thist(j)).select(
                VectorXi::Ones(G.edges[abs(sed.edgeid[i])-1].cumd.size()-1), 0).maxCoeff(&ind);
            
            for (d=0; d<G.nodes.cols(); d++) {
                cumm = G.edges[abs(sed.edgeid[i])-1].cumd(ind+1)-G.edges[abs(sed.edgeid[i])-1].cumd(ind);
                if (cumm==0) cumm=1;
                p(d,pind) = G.edges[abs(sed.edgeid[i])-1].path(d,ind) + (G.edges[abs(sed.edgeid[i])-1].path(d,ind+1)-G.edges[abs(sed.edgeid[i])-1].path(d,ind)) *
                    (thist(j)-G.edges[abs(sed.edgeid[i])-1].cumd(ind))/(cumm);
            }
            
            pind++;
        }
    }
    
    return p;
}

Params readParamFile(char *filename)
{
    Params params;
    
    string line, variable;
    ifstream myfile (filename);
    size_t i;
    vector<string> tokens;
    
    VectorXi nctrlpts;
    
    if (myfile.is_open())
    {
        while ( myfile.good() )
        {
            getline (myfile,line);
            if (line.length()>2 && line[0] != '#') {
                i = line.find_first_of(':');
                variable = line.substr(0, i);
                
                if(variable.compare("epsilon_h")==0)
                {
                    params.epsilon_h = atof(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("epsilon_T")==0)
                {
                    params.epsilon_T = atof(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("K")==0)
                {
                    params.K = atoi(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("kappa")==0)
                {
                    params.kappa = atof(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("gamma")==0)
                {
                    params.gamma = atof(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("N")==0)
                {
                    params.N = atoi(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("N_exp")==0)
                {
                    params.N_exp = atoi(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("N_sim")==0)
                {
                    params.N_sim = atoi(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("N_fine")==0)
                {
                    params.N_fine = atoi(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("termDepth")==0)
                {
                    params.termDepth = atoi(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("timeLimit")==0)
                {
                    params.timeLimit = atof(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("simulateAll")==0)
                {
                    if (atoi(line.substr(i+2,line.length()-i-2).c_str())==1) params.simulateAll = true;
                    else params.simulateAll = false;
                }
                else if(variable.compare("sigma_r")==0)
                {
                    istringstream iss(line.substr(i+2,line.length()-i-2).c_str());
                    tokens.clear();
                    copy(istream_iterator<string>(iss),
                             istream_iterator<string>(),
                             back_inserter<vector<string> >(tokens));
                    params.sigma_r = VectorXd(tokens.size());
                    for (int i=0; i<tokens.size(); i++)
                        params.sigma_r(i) = atof(tokens[i].c_str());
                }
                else if(variable.compare("nctrlpts")==0)
                {
                    istringstream iss(line.substr(i+2,line.length()-i-2).c_str());
                    tokens.clear();
                    copy(istream_iterator<string>(iss),
                             istream_iterator<string>(),
                             back_inserter<vector<string> >(tokens));
                    nctrlpts = VectorXi(tokens.size());
                    for (int i=0; i<tokens.size(); i++)
                        nctrlpts(i) = atoi(tokens[i].c_str());
                    
                    MatrixXd grs(tokens.size(),2);
                    for (int i=0; i<tokens.size(); i++)
                    {
                        grs(i,0) = -2;
                        grs(i,1) = 2;
                    }
                    params.ctrl = controlPoints(grs, nctrlpts);
                }
                else if(variable.compare("P0_ini")==0)
                {
                    params.P0_ini = atof(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("P_initialization")==0)
                {
                    params.P_initialization = atoi(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("d_m")==0)
                {
                    params.d_m = atof(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("transfmodel")==0)
                {
                    params.transfModel = atoi(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("modelcheck")==0)
                {
                    params.modelCheck = atoi(line.substr(i+2,line.length()-i-2).c_str());
                }
                else if(variable.compare("ordering")==0)
                {
                    params.ordering = atoi(line.substr(i+2,line.length()-i-2).c_str());
                }
//                else if(variable.compare("initTree")==0)
//                {
//                    if (atoi(line.substr(i+2,line.length()-i-2).c_str())==1) params.initTree = true;
//                    else params.initTree = false;
//                }
//                else if(variable.compare("jumpProb")==0)
//                {
//                    params.jumpProb = atof(line.substr(i+2,line.length()-i-2).c_str());
//                }
            }
        }
        myfile.close();
        
        if ((params.sigma_r.array() == params.sigma_r(1)).all())
            params.noise_dim_independent = true;
        else params.noise_dim_independent = false;
    }
    
    return params;
}

MatrixXd controlPoints(MatrixXd grid_size, VectorXi nctrlpts)
{
    MatrixXd xm = RowVectorXd::LinSpaced(nctrlpts(0), grid_size(0,0), grid_size(0,1)).replicate(nctrlpts(1),1);
    MatrixXd ym = VectorXd::LinSpaced(nctrlpts(1), grid_size(1,0), grid_size(1,1)).replicate(1,nctrlpts(0));
    
    VectorXd x(Map<VectorXd>(xm.data(), xm.cols()*xm.rows()));
    VectorXd y(Map<VectorXd>(ym.data(), ym.cols()*ym.rows()));
    
    MatrixXd ctrl(x.size(),2); ctrl << x, y;
    
    if(grid_size.rows()==3)
    {
        VectorXd zm = VectorXd::LinSpaced(nctrlpts(2), grid_size(2,0), grid_size(2,1));
        MatrixXd newctrl(ctrl.rows()*zm.size(),3);
        for (int i=0; i<zm.size(); i++)
        {
            newctrl.block(i*ctrl.rows(), 0, ctrl.rows(), 2) = ctrl;
            newctrl.block(i*ctrl.rows(), 2, ctrl.rows(), 1) =  MatrixXd::Ones(ctrl.rows(), 1)*zm(i);
        }
        ctrl = newctrl;
    }
    
    return ctrl;
}

MatrixXd readPointsFromSwc(char *filename)
{
    MatrixXd points;
    vector<string> tokens;
    
    string line, variable;
    ifstream myfile (filename);
    int cnt=0;
    if (myfile.is_open())
    {
        while ( myfile.good() )
        {
            getline (myfile,line);
            if (line.length()>2)
            {
                if (line[0]=='#')
                {
                    if (line.length()>4 && line.substr(1, 4).compare("Size")==0)
                    {
                        int size = atoi(line.substr(6, line.length()-6).c_str());
                        points = MatrixXd(size,3);
                    }
                    continue;
                }
                
                istringstream iss(line);
                tokens.clear();
                copy(istream_iterator<string>(iss),
                         istream_iterator<string>(),
                         back_inserter<vector<string> >(tokens));
                points(cnt,0) = atof(tokens[2].c_str());
                points(cnt,1) = atof(tokens[3].c_str());
                points(cnt,2) = atof(tokens[4].c_str());
                cnt++;
            }
        }
    }
    myfile.close();
    return points;
}

MatrixXi readCompl_Corr(char *filename)
{
    MatrixXi Compl_Corr;
    vector<string> tokens;
    
    string line, variable;
    ifstream myfile (filename);
    int cnt=0;
    if (myfile.is_open())
    {
        while ( myfile.good() )
        {
            getline (myfile,line);
            if (line.length()>2)
            {
                if (line[0]=='#')
                {
                    if (line.length()>4 && line.substr(1, 4).compare("Size")==0)
                    {
                        int size = atoi(line.substr(6, line.length()-6).c_str());
                        Compl_Corr = MatrixXi(size,2);
                    }
                    continue;
                }
                
                istringstream iss(line);
                tokens.clear();
                copy(istream_iterator<string>(iss),
                         istream_iterator<string>(),
                         back_inserter<vector<string> >(tokens));
                Compl_Corr(cnt,0) = atoi(tokens[0].c_str());
                Compl_Corr(cnt,1) = atoi(tokens[1].c_str());
                cnt++;
            }
        }
    }
    myfile.close();
    return Compl_Corr;
}

void copySwcWritePoints(char *originalSwcName, char *outputFile, MatrixXd points)
{
    vector<string> tokens;
    
    ofstream outfile (outputFile);
    string line, variable;
    ifstream myfile (originalSwcName);
    int cnt=0;
    if (myfile.is_open())
    {
        while ( myfile.good() )
        {
            getline (myfile,line);
            if (line.length()>2)
            {
                if (line[0]=='#')
                    continue;
                
                istringstream iss(line);
                tokens.clear();
                copy(istream_iterator<string>(iss),
                         istream_iterator<string>(),
                         back_inserter<vector<string> >(tokens));
                
                if(points.cols()==2)
                    outfile << tokens[0] << " " << tokens[1] << " " << points(cnt,0) << " " << points(cnt,1) << " " << tokens[4] << " " << tokens[5] << " " << tokens[6] << "\n";
                else
                    outfile << tokens[0] << " " << tokens[1] << " " << points(cnt,0) << " " << points(cnt,1) << " " << points(cnt,2) << " " << tokens[5] << " " << tokens[6] << "\n";
                
                cnt++;
            }
        }
    }
    myfile.close();
    outfile.close();
}

Graph readGraph(char *filename, Params params)
{
    Graph graph;
    vector<string> tokens;
    
    string line, variable;
    ifstream myfile (filename);
    int d=0;
    if (myfile.is_open())
    {
        while ( myfile.good() )
        {
            getline (myfile,line);
            if (line.length()>2)
            {
                if(line.compare("#Nodes")==0)
                {
                    getline (myfile,line);
                    istringstream iss(line);
                    copy(istream_iterator<string>(iss),
                             istream_iterator<string>(),
                             back_inserter<vector<string> >(tokens));
                    graph.nodes = MatrixXd(tokens.size(), graph.D);
                    for (int i=0; i<tokens.size(); i++) {
                        graph.nodes(i,0) = atof(tokens[i].c_str());
                    }
                    d++;
                    getline (myfile,line);
                    while (line.length()>2)
                    {
                        istringstream iss(line);
                        tokens.clear();
                        copy(istream_iterator<string>(iss),
                                 istream_iterator<string>(),
                                 back_inserter<vector<string> >(tokens));
                        
                        for (int i=0; i<tokens.size(); i++) {
                            graph.nodes(i,d) = atof(tokens[i].c_str());
                        }
                        d++;
                        getline (myfile,line);
                    }
                }
                else if(line.compare("#Dim")==0)
                {
                    getline (myfile,line);
                    graph.D = atoi(line.c_str());
                }
                else if(line.compare("#Edges")==0)
                {
                    getline (myfile,line);
                    int len = atoi(line.c_str());
                    
                    getline (myfile,line);
                    for (int i=0; i<len; i++)
                    {
                        Edge ed;
                        ed.endpoints = VectorXi(2);
                        getline (myfile,line);
                        istringstream iss(line);
                        tokens.clear();
                        copy(istream_iterator<string>(iss),
                                 istream_iterator<string>(),
                                 back_inserter<vector<string> >(tokens));
                        ed.endpoints(0) = atoi(tokens[0].c_str())-1;
                        ed.endpoints(1) = atoi(tokens[1].c_str())-1;
                        
                        getline (myfile,line);
                        iss.clear();
                        iss.str(line);
                        tokens.clear();
                        copy(istream_iterator<string>(iss),
                                 istream_iterator<string>(),
                                 back_inserter<vector<string> >(tokens));
                        ed.cumd = VectorXd(tokens.size());
                        for (int i=0; i<tokens.size(); i++) {
                            ed.cumd(i) = atof(tokens[i].c_str());
                        }
                        
                        getline (myfile,line);
                        ed.length = atof(line.c_str());
                        
                        getline (myfile,line);
                        iss.clear();
                        iss.str(line);
                        tokens.clear();
                        copy(istream_iterator<string>(iss),
                                 istream_iterator<string>(),
                                 back_inserter<vector<string> >(tokens));
                        ed.path = MatrixXd(graph.D,tokens.size());
                        for (int i=0; i<tokens.size(); i++) {
                            ed.path(0,i) = atof(tokens[i].c_str());
                        }
                        for(int d=1; d<graph.D; d++)
                        {
                            getline (myfile,line);
                            iss.clear();
                            iss.str(line);
                            tokens.clear();
                            copy(istream_iterator<string>(iss),
                                     istream_iterator<string>(),
                                     back_inserter<vector<string> >(tokens));
                            for (int i=0; i<tokens.size(); i++) {
                                ed.path(d,i) = atof(tokens[i].c_str());
                            }
                        }
                        getline (myfile,line);
                        
                        graph.edges.push_back(ed);
                    }
                    
                }
                else if(line.compare("#Superedges")==0)
                {
                    getline (myfile,line);
                    int len = atoi(line.c_str());
                    
                    getline (myfile,line);
                    for (int i=0; i<len; i++)
                    {
                        Superedge sed;
                        getline (myfile,line);
                        istringstream iss(line);
                        tokens.clear();
                        copy(istream_iterator<string>(iss),
                                 istream_iterator<string>(),
                                 back_inserter<vector<string> >(tokens));
                        sed.path = VectorXi(tokens.size());
                        for(int i=0; i<tokens.size(); i++) sed.path(i) = atoi(tokens[i].c_str())-1;
                        
                        getline (myfile,line);
                        if(line.length()==1)
                        {
                            sed.edgeid = VectorXi(1);
                            sed.edgeid(0) = atoi(line.c_str());
                        }
                        else
                        {
                            iss.clear();
                            iss.str(line);
                            tokens.clear();
                            copy(istream_iterator<string>(iss),
                                     istream_iterator<string>(),
                                     back_inserter<vector<string> >(tokens));
                            sed.edgeid = VectorXi(tokens.size());
                            for(int i=0; i<tokens.size(); i++) sed.edgeid(i) = atoi(tokens[i].c_str());
                        }
                        
                        getline (myfile,line);
                        sed.length = atof(line.c_str());
                        
                        getline (myfile,line);
                        iss.clear();
                        iss.str(line);
                        tokens.clear();
                        copy(istream_iterator<string>(iss),
                                 istream_iterator<string>(),
                                 back_inserter<vector<string> >(tokens));
                        sed.desc = VectorXd(tokens.size());
                        for(int i=0; i<tokens.size(); i++) sed.desc(i) = atof(tokens[i].c_str());
                        
                        getline (myfile,line);
                        
                        graph.superedges.push_back(sed);
                    }
                }
                /*else if(line.compare("#Hop Matrix")==0)
                {
                    MatrixXi hm = MatrixXi(graph.nodes.rows(),graph.nodes.rows());
                    double dist;
                    for (int i=0; i<graph.nodes.rows(); i++) {
                        getline (myfile,line);
                        istringstream iss(line);
                        tokens.clear();
                        copy(istream_iterator<string>(iss),
                                 istream_iterator<string>(),
                                 back_inserter<vector<string> >(tokens));
                        for (int j=0; j<graph.nodes.rows(); j++) {
                            hm(i,j) = atoi(tokens[j].c_str());
                        }
                    }
                    graph.diffclose = MatrixXi::Zero(graph.nodes.rows(),graph.nodes.rows());
                    for (int i=0; i<graph.nodes.rows(); i++)
                    for (int j=0; j<graph.nodes.rows(); j++)
                    {
                        dist = sqrt((graph.nodes.row(i)-graph.nodes.row(j))*(graph.nodes.row(i)-graph.nodes.row(j)).transpose());
                        if (dist < params.d_c && hm(i,j) == -1)
                            graph.diffclose(i,j) = 1;
                    }
                }//*/
                else if(line.compare("#FNodes")==0)
                {
                    graph.fnodes = VectorXi(graph.nodes.rows());
                    getline (myfile,line);
                    istringstream iss(line);
                    tokens.clear();
                    copy(istream_iterator<string>(iss),
                             istream_iterator<string>(),
                             back_inserter<vector<string> >(tokens));
                    for (int j=0; j<graph.nodes.rows(); j++) {
                        graph.fnodes(j) = atoi(tokens[j].c_str());
                    }
                }
            }
        }
        myfile.close();
    }
    
    return graph;
}

MatrixXi readPs(char *filename)
{
    MatrixXi Ps;
    vector<string> tokens;
    
    string line, variable;
    ifstream myfile (filename);
    int i;
    if (myfile.is_open())
    {
        while ( myfile.good() )
        {
            getline (myfile,line);
            if (line.length()>2)
            {
                if(line.compare("#Ps")==0)
                {
                    getline (myfile,line);
                    int len = atoi(line.c_str());
                    
                    Ps = MatrixXi(len,2);
                    getline (myfile,line);
                    for (i=0; i<len; i++)
                    {
                        getline (myfile,line);
                        istringstream iss(line);
                        tokens.clear();
                        copy(istream_iterator<string>(iss),
                                 istream_iterator<string>(),
                                 back_inserter<vector<string> >(tokens));
                        Ps(i,0) = atoi(tokens[0].c_str())-1;
                        Ps(i,1) = atoi(tokens[1].c_str())-1;
                    }
                }
            }
        }
    }
    return Ps;
}












