//
//  UniqueMv.h
//  mcts
//
//  Created by Miguel Amável Pinheiro on 24/11/14.
//

#ifndef mcts_UniqueMv_h
#define mcts_UniqueMv_h

#include <iostream>
#include <Dense>
#include <vector>
#include "MCT.h"

using namespace Eigen;
using namespace std;

/**
  struct to handle duplicate matchings between superedeges
*/

class UniqueMs
{
    public:
      vector<MatrixXi> Matches;
      vector<int> NodeIndex;
    
    int isUnique(MatrixXi, int);
};

void quicksort(MatrixXi &Ms, int i, int k);

#endif
