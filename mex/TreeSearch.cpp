//
//  TreeSearch.cpp
//  mcts
//
//  Created by Miguel Amável Pinheiro on 18/6/14.
//

#include "TreeSearch.h"

#ifdef TIME_COMPUTENODE_EVAL
ofstream teval_file_dubchk;
ofstream teval_file_eps;
ofstream teval_file_kup;
ofstream teval_file_upmodel;
ofstream teval_file_sesel;
#endif

struct triple
{
    int index;
    int superedgeSize;
    double length;
};

bool prioritySort(const triple &lhs, const triple &rhs)
{
    if(lhs.superedgeSize == rhs.superedgeSize) return lhs.length > rhs.length; else return lhs.superedgeSize < rhs.superedgeSize;
}

bool sortByQtilde(const pair<int,double>&lhs, const pair<int,double>&rhs)
{
    if(lhs.second == rhs.second) return lhs.first < rhs.first; else return lhs.second > rhs.second;
}


void printNode(Node &node, int digit_len, Graph &graphA, MatrixXi Compl_Corr)
{
    printf("Mv: ");
    for(int k=0; k<node.Mv.cols(); k++)
        printf("%0*d>%0*d ",digit_len, node.Mv(0,k)+1, digit_len, node.Mv(1,k)+1); printf("\nMs: ");
    printf("%*s %*s ", digit_len, " ", digit_len, " ");
    for(int k=0; k<node.Ms.cols(); k++)
        printf("%0*d>%0*d ",digit_len, node.Ms(0,k)+1, digit_len, node.Ms(1,k)+1); printf("\n");
    
    if(Compl_Corr.rows()>0)
    {
        for(int k=0; k<node.Mv.cols(); k++)
        {
            int j=-1;
            for(int i=0; i<Compl_Corr.rows(); i++) if(Compl_Corr(i,0)==node.Mv(0,k)+1) {j=i; break;}
            
            if(j>0 && Compl_Corr(j,1)==node.Mv(1,k)+1)
            {
                if (k>0)
                {
                    printf("%.*f", 2*digit_len-3, graphA.superedges[node.Ms(0,k-1)].length);
                    if (graphA.superedges[node.Ms(0,k-1)].edgeid(0)==0) printf("<");
                    else printf(">");
                    printf("1 ");
                }
                else
                    printf("    %*s %*s ", digit_len, " ", digit_len, " ");
            }
            else
            {
                if (k>0) 
                {
                    printf("%.*f", 2*digit_len-3, graphA.superedges[node.Ms(0,k-1)].length);
                    if (graphA.superedges[node.Ms(0,k-1)].edgeid(0)==0) printf("<");
                    else printf(">");
                    printf("0 ");
                }
                else
                    printf("    %*s %*s ", digit_len, " ", digit_len, " ");
            }
        }
        printf("\n");
    }
    
    printf("\n");
}

Node runTreeSearch(Graph graphA, Graph graphB, Params &params, char *outputFile, MatrixXi Compl_Corr)
{
    if(params.verbose>0) printf("Running Tree Search...\n");
    
    #if OUTPUT_TIMES
//    teval_file_dubchk.open("/Users/amavel/Documents/treematching/methods/time_eval_duplicatecheck.txt");
//    teval_file_eps.open("/Users/amavel/Documents/treematching/methods/time_eval_eps.txt");
//    teval_file_kup.open("/Users/amavel/Documents/treematching/methods/time_eval_kup.txt");
//    teval_file_upmodel.open("/Users/amavel/Documents/treematching/methods/time_eval_updatemodel.txt");
//    teval_file_sesel.open("/Users/amavel/Documents/treematching/methods/time_eval_seselect.txt");
    teval_file_dubchk.open("/datagrid/personal/amavemig/treematching/methods/time_eval_duplicatecheck.txt");
    teval_file_eps.open("/datagrid/personal/amavemig/treematching/methods/time_eval_eps.txt");
    teval_file_kup.open("/datagrid/personal/amavemig/treematching/methods/time_eval_kup.txt");
    teval_file_upmodel.open("/datagrid/personal/amavemig/treematching/methods/time_eval_updatemodel.txt");
    teval_file_sesel.open("/datagrid/personal/amavemig/treematching/methods/time_eval_seselect.txt");
    #endif
    
    /* initialize random seed: */
    srand ((int)time(NULL));
    
    MCST mct;
//    mct.nodes.reserve(params.N);
//    mct.parent.reserve(params.N);
    
    UniqueMs MsList;
    MsList.Matches.reserve(params.N);
    MsList.NodeIndex.reserve(params.N);
    
    Node root;
    int urgent, digit_len = max(ceil(log10(max(graphA.nodes.rows(),graphB.nodes.rows()))),ceil(log10(max(graphA.superedges.size(),graphB.superedges.size()))));
    char filename[128];
    
    root.Q = 0; root.Qplus = 0; root.Qtilde = 0;
    root.Mv = MatrixXi(0,0); root.Ms = MatrixXi(0,0);
    root.possiblept = MatrixXi::Zero(1, 2); root.possiblese = MatrixXi(0,0);
    root.seexpanded = VectorXi(1);
    root.priority = VectorXi(1);
    root.skippeda = VectorXi(1); root.skippedb = VectorXi(1);
    root.visited = 1;
    root.notUnique = -1;
    
    mct.nodes.push_back(root);
    mct.parent.push_back(-1);
    
    /* calculate maximum reward */
    double avgLen, Qnorm, totLenA=0, totLenB=0;
    for(int i=0; i<graphA.edges.size(); i++) totLenA += graphA.edges[i].length;
    for(int i=0; i<graphB.edges.size(); i++) totLenB += graphB.edges[i].length;
    
    avgLen = (totLenA/graphA.edges.size()+totLenB/graphB.edges.size())/2;
    Qnorm = (totLenA+totLenB)/2 + params.kappa * avgLen * min(graphA.nodes.rows(), graphB.nodes.rows());
    
    // Print info on graphs
    if(params.verbose>0)
    {
        printf("Graph A: %ld nodes, %lu superedges\n", graphA.nodes.rows(), graphA.superedges.size());
        printf("Graph B: %ld nodes, %lu superedges\n", graphB.nodes.rows(), graphB.superedges.size());
        printf("avgLen: %f, Qnorm: %f\n\n", avgLen, Qnorm);
    }
    
    /* ................:::::::::::::::: Main Cycle ::::::::::::::::................ */
    struct timeval begin, end, main_clock;
    gettimeofday(&main_clock, NULL);
    vector<int> expanded, simulated;
    int n=1, bestSimulated=0, rootInd=0;
    double bestQ;
    
    while (n<params.N)
    {
        /* Selection */
        gettimeofday(&begin, NULL);
        urgent = select(mct, rootInd, n, Qnorm, params);
        gettimeofday(&end, NULL);
        
        if (urgent==-1) urgent = 0;
        mct.nodes[urgent].visited++;
        
        if(params.verbose>0)
        {
            printf("\nIteration %d - Node %d (depth %ld) selected in %fs (%f,%f)\n",
                n, urgent, mct.nodes[urgent].Ms.cols(), gettimediff(end, begin), mct.nodes[urgent].Qtilde, mct.nodes[0].Qtilde);
            if (urgent==0)
                printf("Root selected. ");
            else {
                printNode(mct.nodes[urgent], digit_len, graphA, Compl_Corr);
                printf("%d possible expandable superedge pairs\n", (1-mct.nodes[urgent].seexpanded.array()).sum());
            }
        }
        
        /* Expansion */
        expanded.clear();
        gettimeofday(&begin, NULL);
        expand(mct, urgent, expanded, n, graphA, graphB, MsList, avgLen, Qnorm, params);
        gettimeofday(&end, NULL);
        #if IS_TEST
        if (!expanded.empty()) {printf("(%d",expanded[0]); for(int k=1; k<expanded.size(); k++) printf(",%d", expanded[k]); printf(") ");}
        #endif
        if(params.verbose>0) printf("%fs\n", gettimediff(end, begin));
        
        if ( mct.nodes[mct.nodes.size()-1].Mv.cols() >= params.termDepth ) {printf("Reached depth of termination criteria after %d iterations\n", n); break;}
        if ( urgent==0 && expanded.size()==0 ) {printf("Search tree exhausted - No more expandable nodes from root after %d iterations\n", n); break;}
        
        if (params.modelCheck==1 && expanded.empty())
            continue;

        /* Simulation */
        simulated.clear();
        gettimeofday(&begin, NULL);
        simulate(mct, expanded, simulated, n, graphA, graphB, MsList, avgLen, Qnorm, params);
        gettimeofday(&end, NULL);
        #if IS_TEST
        if (!simulated.empty()) {printf("(%d",simulated[0]); for(int k=1; k<simulated.size(); k++) printf(",%d", simulated[k]); printf(") ");}
        #endif
        if(params.verbose>0) printf("%fs\n", gettimediff(end, begin));
        
        //For visualization
        bestQ=0;
        if (simulated.empty()) simulated = expanded;
        for (int i=0; i<simulated.size(); i++)
            if (mct.nodes[simulated[i]].Q>bestQ)
                bestSimulated = simulated[i];
        
        if ( mct.nodes[mct.nodes.size()-1].Mv.cols() >= params.termDepth ) {
            printf("Reached depth of termination criteria after %d iterations\n", n);
            break;
        }
        gettimeofday(&end, NULL);
        //printf("Current time %f, limit %f\n", gettimediff(end, main_clock), params.timeLimit);
        if ( gettimediff(end, main_clock) > params.timeLimit ) {
            printf("Reached maximum time allowed (%.3fs)\n", params.timeLimit);
            break;
        }
    }
    if (n>=params.N) printf("Reached maximum number of iterations\n");
    /* ................::::::::::::::::::::::::::::::::::::::::::::................ */
    
    gettimeofday(&end, NULL);
    
    // Find best node
    int indQ=0, dD_Q=0, indD=0, dD=0;
    double dQ=0;
    for (int k=0; k<mct.nodes.size(); k++)
    {
        if (mct.nodes[k].Q>dQ)
        {
            dQ = mct.nodes[k].Q;
            indQ = k;
            dD_Q = (int)mct.nodes[k].Mv.cols();
        } else if (mct.nodes[k].Q==dQ && mct.nodes[k].Mv.cols()>dD_Q)
        {
            dQ = mct.nodes[k].Q;
            indQ = k;
            dD_Q = (int)mct.nodes[k].Mv.cols();
        }
        if (mct.nodes[k].Mv.cols()>dD)
        {
            dD = (int)mct.nodes[k].Mv.cols();
            indD = k;
        }
    }
    
    if(params.verbose>0)
    {
        printf("Best solutions: Node %d (%f) - %ld matches\n", indQ, mct.nodes[indQ].Q/Qnorm, mct.nodes[indQ].Mv.cols());
        for(int k=0; k<mct.nodes[indQ].Mv.cols(); k++) printf("%0*d ", digit_len, mct.nodes[indQ].Mv(0,k)+1); printf("\n");
        for(int k=0; k<mct.nodes[indQ].Mv.cols(); k++) printf("%0*d ", digit_len, mct.nodes[indQ].Mv(1,k)+1); printf("\n");
        
        if(Compl_Corr.rows()>0)
        {
            for(int k=0; k<mct.nodes[indQ].Mv.cols(); k++)
            {
                int j=-1;
                for(int i=0; i<Compl_Corr.rows(); i++) if(Compl_Corr(i,0)==mct.nodes[indQ].Mv(0,k)+1) {j=i; break;}
                if(j>0 && Compl_Corr(j,1)==mct.nodes[indQ].Mv(1,k)+1) printf("%0*d ", digit_len, 1);
                else printf("%0*d ", digit_len, 0);
            }
            printf("\n");
        }
        
        printf("\nWith corresponding superedges\n");
        printf("%0*d ", digit_len, 0); for(int k=0; k<mct.nodes[indQ].Ms.cols(); k++) printf("%0*d ", digit_len, mct.nodes[indQ].Ms(0,k)+1); printf("\n");
        printf("%0*d ", digit_len, 0); for(int k=0; k<mct.nodes[indQ].Ms.cols(); k++) printf("%0*d ", digit_len, mct.nodes[indQ].Ms(1,k)+1); printf("\n\n");
        
        printf("%ld possible exapandable superedge pairs from this node. ", mct.nodes[indQ].possiblese.rows());
        for(int k=0; k<mct.nodes[indQ].possiblese.rows(); k++)
            printf("%d>%d ", mct.nodes[indQ].possiblept(k,0)+1, mct.nodes[indQ].possiblept(k,1)+1); printf("\n");
        printf("Solution found in %fs\n\n", gettimediff(end, main_clock));
    }
    
    //Write solution to file
    strcpy(filename, outputFile);
    writeResult(filename, mct.nodes[indQ], gettimediff(end, main_clock));
    
    mct.nodes[indQ].time_elapsed = gettimediff(end, main_clock);
    
    
    #if OUTPUT_TIMES
    teval_file_dubchk.close();
    teval_file_eps.close();
    teval_file_kup.close();
    teval_file_upmodel.close();
    teval_file_sesel.close();
    #endif
    
    return mct.nodes[indQ];
}

/*******************************************************************************************/
/* Select - select most urgent node to expand and simulate. Connected with highest tilde_Q */
/*******************************************************************************************/
int select(MCST &mct, int &node_p, int &it, double &Qnorm, Params &params)
{
    int i, node_cur;
    vector<pair<int,double> > childrenrew;
    
    // if not expandable, forget it
    if (mct.nodes[node_p].children.size()==0 && (mct.nodes[node_p].possiblese.rows()==0 || (mct.nodes[node_p].seexpanded.array()==1).all()))
        {mct.nodes[node_p].not_expandable = true; return -1;}
    
    // Recalculate Qtilde and put them in a vector for sorting
    if (node_p==0) mct.nodes[node_p].Qtilde = mct.nodes[node_p].Qplus/Qnorm + params.gamma * sqrt( (2*log(it))/mct.nodes[node_p].visited );
    for (i=0; i<mct.nodes[node_p].children.size(); i++)
    {
        mct.nodes[mct.nodes[node_p].children[i]].Qtilde = mct.nodes[mct.nodes[node_p].children[i]].Qplus/Qnorm + params.gamma * sqrt( (2*log(it))/mct.nodes[mct.nodes[node_p].children[i]].visited );
        //mct.nodes[nu_p.children[i]].visited++;
        
        if (!mct.nodes[mct.nodes[node_p].children[i]].not_expandable)
            childrenrew.push_back(pair<int, double>(mct.nodes[mct.nodes[node_p].children[i]].index,mct.nodes[mct.nodes[node_p].children[i]].Qtilde));
    }
    #if SELECTION_DEBUG
    if (node_p>0)
        cout << node_p << "," << mct.nodes[node_p].Mv.col(mct.nodes[node_p].Mv.cols()-1).transpose().array()+1 << "," << mct.nodes[node_p].Qtilde << "," << childrenrew.size() << endl;
    else
        cout << node_p << "," << mct.nodes[node_p].Qtilde << "," << childrenrew.size() << endl;
    #endif
    if (childrenrew.empty())
    {
        #if SELECTION_DEBUG
        cout << node_p << ">" << (mct.nodes[node_p].seexpanded.size()==0) << ">" << (mct.nodes[node_p].seexpanded.array()==1).all() << endl;
        cout << mct.nodes[node_p].seexpanded.transpose() << endl;
        #endif
        if ((mct.nodes[node_p].seexpanded.array()==1).all()) {mct.nodes[node_p].not_expandable = true; return -1;}
        return node_p;
    }
    
    sort(childrenrew.begin(), childrenrew.end(), sortByQtilde);
    
    // Recursively select the child with best Qtilde, backtrack if parent is higher
    node_cur = node_p;
    for (i=0; i<childrenrew.size(); i++)
    {
        node_cur = select(mct, childrenrew[i].first, it, Qnorm, params);
        if (node_cur!=-1)
        {
            #if SELECTION_DEBUG
            cout << node_p << "," << node_cur << "," << (mct.nodes[node_cur].Qtilde >= mct.nodes[node_p].Qtilde) << "," << mct.nodes[node_cur].Qtilde << "," << mct.nodes[node_p].Qtilde << "," << (mct.nodes[node_p].seexpanded.size()==0) << "," << (mct.nodes[node_p].seexpanded.array()==1).all() << endl;
            #endif
            if (mct.nodes[node_cur].Qtilde >= mct.nodes[node_p].Qtilde)// || nu_p.seexpanded.size()==0 || (nu_p.seexpanded.array()==1).all())
                return node_cur;
            else if (mct.nodes[node_p].seexpanded.size()==0 || (mct.nodes[node_p].seexpanded.array()==1).all())
                return -1;
//            else return node_p;
        }
    }
    
    //if no child is selected return parent, or -1 if it is not expandable
    if(mct.nodes[node_p].seexpanded.size()!=0 && !(mct.nodes[node_p].seexpanded.array()==1).all())
        return node_p;
    else {mct.nodes[node_p].not_expandable = true; return -1;}
}

/*******************************************************************************************/
/**************** Expand - add N_exp children to the node with index urgent ****************/
/*******************************************************************************************/
void expand(MCST &mct, int urgent, vector<int> &expanded, int &n, Graph &graphA, Graph &graphB, UniqueMs &MsList, double avgLen, double Qnorm, Params &params)
{
    Node tmp;
    vector<int> possiblea1, possibleptsa1, possiblea2, possibleptsa2;
    vector<int> possibleb1, possibleptsb1, possibleb2, possibleptsb2;
    Node node, parent_node;
    int s, c;
    vector<triple> superedgesToSort;
    #if TIME_EXPANSION_EVAL || OUTPUT_TIMES
    struct timeval begin, end;
    #endif
    
    parent_node = mct.nodes[urgent];
    
    int cnt=0;
    if (urgent==0) /* Root node */
    {
        for (int k=0; k<params.N_exp; k++)
        {
            #if TIME_EXPANSION_EVAL
            gettimeofday(&begin, NULL);
            #endif
            s = selectRootSuperedge(mct.nodes[urgent], graphA, graphB, params);
            #if IS_TEST
            if(params.verbose>0)
            {
                printf("(%d>%d, %d>%d %d>%d)\n", mct.nodes[urgent].possiblese(s,0)+1, mct.nodes[urgent].possiblese(s,1)+1,
                    graphA.superedges[mct.nodes[urgent].possiblese(s,0)].path(0)+1, graphB.superedges[mct.nodes[urgent].possiblese(s,1)].path(0)+1,
                    graphA.superedges[mct.nodes[urgent].possiblese(s,0)].path(graphA.superedges[mct.nodes[urgent].possiblese(s,0)].path.size()-1)+1,
                    graphB.superedges[mct.nodes[urgent].possiblese(s,1)].path(graphB.superedges[mct.nodes[urgent].possiblese(s,1)].path.size()-1)+1);
            }
            #endif
            #if TIME_EXPANSION_EVAL
            gettimeofday(&end, NULL); printf("RootExpansion-Select superedge pair %f\n", gettimediff(end, begin));
            #endif
            if (s==-1) break;
            
            tmp = mct.nodes[0];
            tmp.children.clear();
            tmp.Mv = MatrixXi(2, 2);
            tmp.Mv(0,0) = graphA.superedges[mct.nodes[urgent].possiblese(s,0)].path(0);
            tmp.Mv(1,0) = graphB.superedges[mct.nodes[urgent].possiblese(s,1)].path(0);
            tmp.Mv(0,1) = graphA.superedges[mct.nodes[urgent].possiblese(s,0)].path(graphA.superedges[mct.nodes[urgent].possiblese(s,0)].path.size()-1);
            tmp.Mv(1,1) = graphB.superedges[mct.nodes[urgent].possiblese(s,1)].path(graphB.superedges[mct.nodes[urgent].possiblese(s,1)].path.size()-1);
            tmp.Ms = MatrixXi(2, 1); tmp.Ms << mct.nodes[urgent].possiblese(s,0), mct.nodes[urgent].possiblese(s,1);
            tmp.visited = 1;
            tmp.Qplus = 0;
            
            tmp.skippeda = graphA.superedges[tmp.Ms(0,0)].path.segment(1, graphA.superedges[tmp.Ms(0,0)].path.size()-1);
            tmp.skippedb = graphB.superedges[tmp.Ms(1,0)].path.segment(1, graphB.superedges[tmp.Ms(1,0)].path.size()-1);
            
            #if TIME_EXPANSION_EVAL
            gettimeofday(&begin, NULL);
            #endif
            // Collect all adjacent superedges
            possiblea1.clear(); possibleptsa1.clear(); possiblea2.clear(); possibleptsa2.clear();
            for (int a=0; a<graphA.superedges.size(); a++)
            {
                if ((graphA.superedges[a].path(0)==tmp.Mv(0,0) && !(tmp.skippeda.array()==graphA.superedges[a].path(graphA.superedges[a].path.size()-1)).any() &&
                    tmp.Mv(0,1)!=graphA.superedges[a].path(graphA.superedges[a].path.size()-1) && tmp.Ms(0,0)!=a) && ( graphA.superedges[tmp.Ms(0,0)].edgeid(0)==0 ||
                    !(graphA.superedges[a].edgeid.replicate(1, graphA.superedges[tmp.Ms(0,0)].edgeid.size()).array().abs()== //no overlap of edges with Ms
                    graphA.superedges[tmp.Ms(0,0)].edgeid.transpose().replicate(graphA.superedges[a].edgeid.size(), 1).array().abs()).any() ))
                {
                    possiblea1.push_back(a);
                    possibleptsa1.push_back(graphA.superedges[a].path(graphA.superedges[a].path.size()-1));
                }
                if ((graphA.superedges[a].path(0)==tmp.Mv(0,1) && !(tmp.skippeda.array()==graphA.superedges[a].path(graphA.superedges[a].path.size()-1)).any() &&
                    tmp.Mv(0,0)!=graphA.superedges[a].path(graphA.superedges[a].path.size()-1) && tmp.Ms(0,0)!=a) && ( graphA.superedges[tmp.Ms(0,0)].edgeid(0)==0 ||
                    !(graphA.superedges[a].edgeid.replicate(1, graphA.superedges[tmp.Ms(0,0)].edgeid.size()).array().abs()==
                    graphA.superedges[tmp.Ms(0,0)].edgeid.transpose().replicate(graphA.superedges[a].edgeid.size(), 1).array().abs()).any() ))
                {
                    possiblea2.push_back(a);
                    possibleptsa2.push_back(graphA.superedges[a].path(graphA.superedges[a].path.size()-1));
                }
            }
            #if IS_TEST
            printf("ptsa1 "); for (int a=0; a< possibleptsa1.size(); a++) printf(" %d", possibleptsa1[a]+1); printf("\n");
            printf("ptsa2 "); for (int a=0; a< possibleptsa2.size(); a++) printf(" %d", possibleptsa2[a]+1); printf("\n");
            #endif
            
            possibleb1.clear(); possibleptsb1.clear(); possibleb2.clear(); possibleptsb2.clear();
            for (int b=0; b<graphB.superedges.size(); b++)
            {
                if ((graphB.superedges[b].path(0)==tmp.Mv(1,0) && !(tmp.skippedb.array()==graphB.superedges[b].path(graphB.superedges[b].path.size()-1)).any() &&
                    tmp.Mv(1,1)!=graphB.superedges[b].path(graphB.superedges[b].path.size()-1) && tmp.Ms(1,0)!=b) && ( graphB.superedges[tmp.Ms(1,0)].edgeid(0)==0 ||
                    !(graphB.superedges[b].edgeid.replicate(1, graphB.superedges[tmp.Ms(1,0)].edgeid.size()).array().abs()==
                    graphB.superedges[tmp.Ms(1,0)].edgeid.transpose().replicate(graphB.superedges[b].edgeid.size(), 1).array().abs()).any() ))
                {
                    possibleb1.push_back(b);
                    possibleptsb1.push_back(graphB.superedges[b].path(graphB.superedges[b].path.size()-1));
                }
                if ((graphB.superedges[b].path(0)==tmp.Mv(1,1) && !(tmp.skippedb.array()==graphB.superedges[b].path(graphB.superedges[b].path.size()-1)).any() &&
                    tmp.Mv(1,0)!=graphB.superedges[b].path(graphB.superedges[b].path.size()-1) && tmp.Ms(1,0)!=b) && ( graphB.superedges[tmp.Ms(1,0)].edgeid(0)==0 ||
                    !(graphB.superedges[b].edgeid.replicate(1, graphB.superedges[tmp.Ms(1,0)].edgeid.size()).array().abs()==
                    graphB.superedges[tmp.Ms(1,0)].edgeid.transpose().replicate(graphB.superedges[b].edgeid.size(), 1).array().abs()).any() ))
                {
                    possibleb2.push_back(b);
                    possibleptsb2.push_back(graphB.superedges[b].path(graphB.superedges[b].path.size()-1));
                }
            }
            #if IS_TEST
            printf("ptsb1 "); for (int a=0; a< possibleptsb1.size(); a++) printf(" %d", possibleptsb1[a]+1); printf("\n");
            printf("ptsb2 "); for (int a=0; a< possibleptsb2.size(); a++) printf(" %d", possibleptsb2[a]+1); printf("\n");
            #endif
            
            #if TIME_EXPANSION_EVAL
            gettimeofday(&end, NULL); printf("RootExpansion-Superedge Collection %f\n", gettimediff(end, begin));
            gettimeofday(&begin, NULL);
            #endif
            
            // Mesh them up
            MatrixXi possiblese = MatrixXi(possiblea1.size()*possibleb1.size() + possiblea2.size()*possibleb2.size(),2);
            MatrixXi possiblept = MatrixXi(possibleptsa1.size()*possibleptsb1.size() + possibleptsa2.size()*possibleptsb2.size(),2);
            for (int m=0; m<possibleb1.size(); m++)
            for (int a=0; a<possiblea1.size(); a++){
                possiblese(a + m*possiblea1.size(),0) = possiblea1[a];
                possiblese(a + m*possiblea1.size(),1) = possibleb1[m];
                possiblept(a + m*possiblea1.size(),0) = possibleptsa1[a];
                possiblept(a + m*possiblea1.size(),1) = possibleptsb1[m];
            }
            int currentSize = (int)(possiblea1.size()*possibleb1.size());
            for (int m=0; m<possibleb2.size(); m++)
            for (int a=0; a<possiblea2.size(); a++){
                possiblese(currentSize + a + m*possiblea2.size(),0) = possiblea2[a];
                possiblese(currentSize + a + m*possiblea2.size(),1) = possibleb2[m];
                possiblept(currentSize + a + m*possiblea2.size(),0) = possibleptsa2[a];
                possiblept(currentSize + a + m*possiblea2.size(),1) = possibleptsb2[m];
            }
            #if IS_TEST
//            printToMatlabi("pses1", possiblese.transpose());
//            printToMatlabi("ppts1", possiblept.transpose());
            printf("pses1 "); for (int a=0; a< possiblese.rows(); a++) printf(" %03d", possiblese(a,0)+1); printf("\n");
            printf("      "); for (int a=0; a< possiblese.rows(); a++) printf(" %03d", possiblese(a,1)+1); printf("\n");
            printf("ppts1 "); for (int a=0; a< possiblept.rows(); a++) printf(" %03d", possiblept(a,0)+1); printf("\n");
            printf("      "); for (int a=0; a< possiblept.rows(); a++) printf(" %03d", possiblept(a,1)+1); printf("\n");
            printf("      "); for (int a=0; a< possiblept.rows(); a++) printf(" %03d", issecomp(graphA.superedges[possiblese(a,0)].desc, graphB.superedges[possiblese(a,1)].desc, params.epsilon_h)); printf("\n");
            #endif
            
            #if TIME_EXPANSION_EVAL
            gettimeofday(&end, NULL); printf("RootExpansion-Mesh superedges %f\n", gettimediff(end, begin));
            gettimeofday(&begin, NULL);
            #endif
            
            //Delete pairs with not allowable superedge pairs, with different fnodes and only take pairs which are simultaniously virtual edges or not
            for (int m=(int)possiblese.rows()-1; m>=0; m--)
                if (!issecomp(graphA.superedges[possiblese(m,0)].desc, graphB.superedges[possiblese(m,1)].desc, params.epsilon_h) || graphA.fnodes(possiblept(m,0))!=graphB.fnodes(possiblept(m,1)) ||
                    (graphA.superedges[possiblese(m,0)].edgeid(0)==0 && graphB.superedges[possiblese(m,1)].edgeid(0)!=0) ||
                    (graphA.superedges[possiblese(m,0)].edgeid(0)!=0 && graphB.superedges[possiblese(m,1)].edgeid(0)==0)) //edgeid == 0 -> virtual edge -- it connects nodes in different trees which are within d_c
                {
                    removeRow(possiblese, m);
                    removeRow(possiblept, m);
                }
            #if TIME_EXPANSION_EVAL
            gettimeofday(&end, NULL); printf("RootExpansion-Remove incompatible %f\n", gettimediff(end, begin));
            #endif
            
            #if IS_TEST
//            printToMatlabi("pses2", possiblese.transpose());
//            printToMatlabi("ppts2", possiblept.transpose());
            printf("pses2 "); for (int a=0; a< possiblese.rows(); a++) printf(" %03d", possiblese(a,0)+1); printf("\n");
            printf("      "); for (int a=0; a< possiblese.rows(); a++) printf(" %03d", possiblese(a,1)+1); printf("\n");
            printf("ppts2 "); for (int a=0; a< possiblept.rows(); a++) printf(" %03d", possiblept(a,0)+1); printf("\n");
            printf("      "); for (int a=0; a< possiblept.rows(); a++) printf(" %03d", possiblept(a,1)+1); printf("\n");
            #endif
            
            if (possiblese.rows()==0)
            {
                k--;
                #if TIME_EXPANSION_EVAL
                printf("::RootExpansion-Node Cancelled::\n");
                #endif
                continue;
            }

            #if TIME_EXPANSION_EVAL
            gettimeofday(&begin, NULL);
            #endif
            if (params.modelCheck==1)
            {
                superedgesToSort.clear();
                for (int m=0; m<possiblese.rows(); m++)
                {
                    triple se;
                    se.index = m;
                    se.length = graphA.superedges[possiblese(m,0)].length + graphB.superedges[possiblese(m,1)].length;
                    if (graphA.superedges[possiblese(m,0)].edgeid(0)==0) //edgeid == 0 -> virtual edge -- it connects nodes in different trees which are within d_c
                        se.superedgeSize = params.K+1;
                    else
                        se.superedgeSize = (int)max(graphA.superedges[possiblese(m,0)].edgeid.size(), graphB.superedges[possiblese(m,1)].edgeid.size());
                    superedgesToSort.push_back(se);
                }
            
                if (params.ordering == 0)
                    sort(superedgesToSort.begin(), superedgesToSort.end(), prioritySort);
            
                tmp.possiblese = MatrixXi( (int)possiblese.rows(), 2 );
                tmp.possiblept = MatrixXi( (int)possiblese.rows(), 2 );
                for (int m=0; m<possiblese.rows(); m++)
                {
                    tmp.possiblese(m,0) = possiblese(superedgesToSort[m].index,0);
                    tmp.possiblese(m,1) = possiblese(superedgesToSort[m].index,1);
                    tmp.possiblept(m,0) = possiblept(superedgesToSort[m].index,0);
                    tmp.possiblept(m,1) = possiblept(superedgesToSort[m].index,1);
                }
                tmp.priority = VectorXi::Zero(tmp.possiblese.rows());
                tmp.seexpanded = VectorXi::Zero(tmp.possiblese.rows());
            }
            else if (params.modelCheck==0)
            {
                tmp.possiblese = MatrixXi( (int)possiblese.rows(), 2 );
                tmp.possiblept = MatrixXi( (int)possiblese.rows(), 2 );
                tmp.priority = VectorXi( (int)possiblese.rows() );
                for (int m=0; m<possiblese.rows(); m++)
                {
                    tmp.possiblese(m,0) = possiblese(m,0);
                    tmp.possiblese(m,1) = possiblese(m,1);
                    tmp.possiblept(m,0) = possiblept(m,0);
                    tmp.possiblept(m,1) = possiblept(m,1);
                    if (graphA.superedges[possiblese(m,0)].edgeid(0)==0) //edgeid == 0 -> virtual edge -- it connects nodes in different trees which are within d_c
                        tmp.priority(m) = params.K+1;
                    else
                        tmp.priority(m) = (int)max(graphA.superedges[possiblese(m,0)].edgeid.size(), graphB.superedges[possiblese(m,1)].edgeid.size());
                }
                tmp.seexpanded = VectorXi::Zero(tmp.possiblese.rows());
            }
            #if TIME_EXPANSION_EVAL
            gettimeofday(&end, NULL); printf("RootExpansion-Add possible superedges %f\n", gettimediff(end, begin));
            #endif
            
            // Initialize transformation model. epsilon check doesn't need initialization
            if (params.transfModel==1)
            {
                #if TIME_EXPANSION_EVAL
                gettimeofday(&begin, NULL);
                #endif
                //Find initial transformation model
                MatrixXd Tr, crig, mu;
                Tr = findR2(zeta_superedge(graphA, tmp.Ms(0,0), VectorXd::LinSpaced(5, 0, 1)).transpose(), zeta_superedge(graphB, tmp.Ms(1,0), VectorXd::LinSpaced(5, 0, 1)).transpose());
                crig = applyRigidTransformation(params.ctrl, Tr);
                
                //No regularization on the coefficients
                MatrixXd Beta = beta3(params.ctrl.col(0).replicate(1, params.ctrl.rows()) - params.ctrl.col(0).transpose().replicate(params.ctrl.rows(), 1));
                for (int i=1; i<graphA.D; i++) {
                    Beta = Beta.array() * beta3((params.ctrl.col(i).replicate(1, params.ctrl.rows()) - params.ctrl.col(i).transpose().replicate(params.ctrl.rows(), 1))).array();
                }
                mu = Beta.colPivHouseholderQr().solve(crig-params.ctrl);
                
                tmp.trModel.Ct = mu;
                for (int i=0; i<graphA.D; i++) {
                    switch (params.P_initialization) {
                        case 0:
                            tmp.trModel.Pt.push_back( MatrixXd::Identity(Beta.rows(),Beta.cols()) * params.P0_ini );
                            break;
                        case 1:
                            tmp.trModel.Pt.push_back( Beta.array() * params.P0_ini );
                            break;
                        case 2:
                            tmp.trModel.Pt.push_back( MatrixXd::Ones(Beta.rows(),Beta.cols()) * params.P0_ini );
                            break;
                    }
//                    if (params.noise_dim_independent) break;
                }
                #if TIME_EXPANSION_EVAL
                gettimeofday(&end, NULL); printf("RootExpansion-Calculate model %f\n", gettimediff(end, begin));
                #endif
            }
        
            #if IS_TEST
            cout << tmp.Mv.array()+1 << endl;
            printToMatlabi("ppts", tmp.possiblept.transpose());
            #endif
            
            #if TIME_EXPANSION_EVAL
            gettimeofday(&begin, NULL);
            #endif
            //Remove possible superedges which are not compatible with transformation model
            if (params.modelCheck==0 && tmp.possiblept.rows()>0)
                removeModelIncompatible(tmp, graphA, graphB, params);
            
            #if TIME_EXPANSION_EVAL
            gettimeofday(&end, NULL); printf("RootExpansion-Remove model incompatible %f\n", gettimediff(end, begin));
            gettimeofday(&begin, NULL);
            #endif
            
            if (tmp.possiblept.rows()>0) tmp.not_expandable = 0;
            else tmp.not_expandable = 1;
            
            calculateQ(tmp, graphA, graphB, avgLen, params);
            tmp.Qplus = tmp.Q;
            tmp.Qtilde = calculateQtilde(tmp, 1, Qnorm, params);
            
            #if TIME_EXPANSION_EVAL
            gettimeofday(&end, NULL); printf("RootExpansion-Calculate Q %f\n", gettimediff(end, begin));
            gettimeofday(&begin, NULL);
            #endif
            
            mct.nodes[0].children.push_back((int)mct.nodes.size());
            tmp.index = (int)mct.nodes.size();
            mct.nodes.push_back(tmp);
            mct.parent.push_back(0);
            
            if (mct.nodes[0].Qplus<tmp.Q)
            {
                mct.nodes[0].Qplus = tmp.Q;
                mct.nodes[0].Qtilde = tmp.Qtilde;
            }
            
            #if TIME_EXPANSION_EVAL
            gettimeofday(&end, NULL); printf("RootExpansion-Conclude and add to tree %f\n", gettimediff(end, begin));
            gettimeofday(&begin, NULL);
            #endif
            
            backpropagate(mct, (int)mct.nodes.size()-1);
            
            expanded.push_back((int)mct.nodes.size()-1);
            n++; cnt++;
            #if TIME_EXPANSION_EVAL
            gettimeofday(&end, NULL); printf("RootExpansion-Backpropagate %f\n", gettimediff(end, begin));
            #endif
        }
    }
    else
    {
        for (int k=0; k<params.N_exp; k++)
        {
            #if TIME_EXPANSION_EVAL || OUTPUT_TIMES
            gettimeofday(&begin, NULL);
            #endif
            // Select highest priority superedge: the field priority relates to the number of edges in superedge pair and then length is checked
            s = selectSuperedge(mct.nodes[urgent], graphA, graphB, params);
            
            if(params.verbose>0 && k==0 && s!=-1)
            {
                printf("(%d>%d, %d>%d) ", mct.nodes[urgent].possiblese(s,0)+1, mct.nodes[urgent].possiblese(s,1)+1,
                    mct.nodes[urgent].possiblept(s,0)+1, mct.nodes[urgent].possiblept(s,1)+1);
            }
            #if TIME_EXPANSION_EVAL
            gettimeofday(&end, NULL); printf("Expansion-Select superedge %f\n", gettimediff(end, begin));
            #endif
            #if OUTPUT_TIMES
            gettimeofday(&end, NULL); teval_file_sesel << std::fixed << std::setprecision(8) << gettimediff(end, begin) << endl;
            #endif
            if (s==-1)
            {
//                cout << urgent << "," << (mct.nodes[urgent].seexpanded.array()==1).all() << endl;
                mct.nodes[urgent].not_expandable = 1;
//                if ( (mct.nodes[urgent].seexpanded.array()==1).all() ) break;
                break;
            }
            
            node = parent_node;
            #if TIME_EXPANSION_EVAL
            gettimeofday(&begin, NULL);
            #endif
            computeNode(node, (int)mct.nodes.size(), s, n, graphA, graphB, MsList, avgLen, Qnorm, params);
            #if TIME_EXPANSION_EVAL
            gettimeofday(&end, NULL); printf("Expansion-ComputeNode %f\n", gettimediff(end, begin));
            gettimeofday(&begin, NULL);
            #endif
            if(node.notUnique>=0)
            {
                c = node.notUnique;
                node = mct.nodes[c];
            }
            
            mct.nodes[urgent].seexpanded(s) = 1;
            
            if (node.possiblese.size()==0) node.not_expandable = 1;
        
            mct.nodes[urgent].children.push_back(node.index);
            if (node.index==(int)mct.nodes.size()) // not in the tree
            {
                mct.nodes.push_back(node);
                mct.parent.push_back(urgent);
            }
            
            backpropagate(mct, node.index);
            n++;
            
            expanded.push_back(node.index);
            cnt++;
            #if TIME_EXPANSION_EVAL
            gettimeofday(&end, NULL); printf("Expansion-Conclude and add node %f\n", gettimediff(end, begin));
            #endif
            
            if ( mct.nodes[node.index].Mv.cols() == params.termDepth ) return;
        }
    }
    if(params.verbose>0) {
        printf("%d node(s) expanded, ", cnt);
        if (expanded.size()!=0)
            printf("(Q=%f) ", mct.nodes[expanded[0]].Q);
    }
    
    return;
}

/*****************************************************************************************************************/
/* Simulate - add N_sim children to each added node consecutively in a DFS fashion starting from the most urgent */
/*****************************************************************************************************************/
void simulate(MCST &mct, vector<int> expanded, vector<int> &simulated, int &n, Graph &graphA, Graph &graphB, UniqueMs &MsList, double avgLen, double Qnorm, Params &params)
{
    MCST branch;
    int s=0, cnt=0, parent, c;
    VectorXi cnts = VectorXi::Zero(expanded.size());
    Node node;
    #if TIME_SIMULATION_EVAL || OUTPUT_TIMES
    struct timeval begin, end;
    #endif
    
    // Simulate in Depth-First strategy -- recursively add a child to the created nodes
    for (int e=0; e<expanded.size(); e++)
    {
        if (!params.simulateAll && e>0) break;
        node = mct.nodes[expanded[e]];
        parent = expanded[e];
        for (int k=0; k<params.N_sim; k++)
        {
            if (node.not_expandable) break;
            
            #if TIME_SIMULATION_EVAL || OUTPUT_TIMES
            gettimeofday(&begin, NULL);
            #endif
            // Select highest priority superedge: the field priority relates to the number of edges in superedge pair and then length is checked
            s = selectSuperedge(node, graphA, graphB, params);
            
            #if IS_TEST
            if(params.verbose>0 && e==0 && s!=-1)
            {
                printf("(%d>%d, %d>%d) ", mct.nodes[node.index].possiblese(s,0)+1, mct.nodes[node.index].possiblese(s,1)+1,
                    mct.nodes[node.index].possiblept(s,0)+1, mct.nodes[node.index].possiblept(s,1)+1);
            }
            #endif
            #if TIME_SIMULATION_EVAL
            gettimeofday(&end, NULL); printf("Simulation-Select superedge %f\n", gettimediff(end, begin));
            #endif
            #if OUTPUT_TIMES
            gettimeofday(&end, NULL); teval_file_sesel << std::fixed << std::setprecision(8) << gettimediff(end, begin) << endl;
            #endif
            if (s==-1)
            {
                mct.nodes[node.index].not_expandable = 1; k--;
                break;
            }
            
            #if TIME_SIMULATION_EVAL
            gettimeofday(&begin, NULL);
            #endif
            computeNode(node, (int)mct.nodes.size(), s, n, graphA, graphB, MsList, avgLen, Qnorm, params);
            #if TIME_SIMULATION_EVAL
            gettimeofday(&end, NULL); printf("Simulation-Compute Node %f\n", gettimediff(end, begin));
            gettimeofday(&begin, NULL);
            #endif
            if(node.notUnique>=0)
            {
                c = node.notUnique;
                node = mct.nodes[c];
            }
            
            mct.nodes[parent].seexpanded(s) = 1;
            
            if (node.possiblese.size()==0) node.not_expandable = 1;
            
            mct.nodes[parent].children.push_back(node.index);
            if (node.index==(int)mct.nodes.size()) // not in the tree
            {
                mct.nodes.push_back(node);
                mct.parent.push_back(parent);
            }
            
            backpropagate(mct, node.index);
            #if TIME_SIMULATION_EVAL
            gettimeofday(&end, NULL); printf("Simulation-Conclude and backpropagate %f\n", gettimediff(end, begin));
            #endif
            n++; cnt++; cnts(e)++;
            
            parent = node.index;
            simulated.push_back(node.index);
            
            if ( mct.nodes[node.index].Mv.cols() == params.termDepth ) break;
        }
        if ( mct.nodes[node.index].Mv.cols() == params.termDepth ) break;
    }
    if(params.verbose>0)
    {
        printf("%d (%d", cnt, cnts(0));
        for (int e=1; e<cnts.size(); e++) printf(",%d", cnts(e));
        printf(") nodes expanded in simulation, ");
        if (simulated.size()!=0)
            printf("(Q=%f) ", mct.nodes[simulated[simulated.size()-1]].Q);
    }
    
    return;
}

/*****************************************************************************************************************/
/* ComputeNode - action of processing a node by adding a new match: check the superedges attached to the new     */
/*               endpoints, check transformation model compatibility, ...                                        */
/*****************************************************************************************************************/
void computeNode(Node &node, int mctSize, int s, int n, Graph &graphA, Graph &graphB, UniqueMs &MsList, double avgLen, double Qnorm, Params &params)
{
    Node tmp = node;
    tmp.Ms = MatrixXi(2, tmp.Ms.cols()+1);
    tmp.Mv = MatrixXi(2, tmp.Mv.cols()+1);
    vector<triple> superedgesToSort;
    int newSEcol=(int)tmp.Ms.cols()-1;
    #if TIME_COMPUTENODE_EVAL || OUTPUT_TIMES
    struct timeval begin, end;
    gettimeofday(&begin, NULL);
    #endif
    
    int cnt=0;
    for (int i=0; i<node.Mv.cols(); i++)
    {
        
        if (cnt<node.Ms.cols() && (cnt!=i || node.possiblese(s,0)>node.Ms(0,cnt)))
        {
            tmp.Ms(0,i) = node.Ms(0,cnt);
            tmp.Ms(1,i) = node.Ms(1,cnt);
            cnt++;
        }
        else
        {
            newSEcol = i;
            tmp.Ms(0,i) = node.possiblese(s,0);
            tmp.Ms(1,i) = node.possiblese(s,1);
        }
        
        tmp.Mv(0,i) = node.Mv(0,i);
        tmp.Mv(1,i) = node.Mv(1,i);
    }
    
//    tmp.Ms(0,node.Ms.cols()) = node.possiblese(s,0);
//    tmp.Ms(1,node.Ms.cols()) = node.possiblese(s,1);
    
    tmp.Mv(0,node.Mv.cols()) = node.possiblept(s,0);
    tmp.Mv(1,node.Mv.cols()) = node.possiblept(s,1);
    
    #if TIME_COMPUTENODE_EVAL
    gettimeofday(&end, NULL);
    printf("ComputeNode-Cloning match %f\n", gettimediff(end, begin));
    #endif
    
    #if TIME_COMPUTENODE_EVAL || OUTPUT_TIMES
    gettimeofday(&begin, NULL);
    #endif
    int isuniq = MsList.isUnique(tmp.Ms, mctSize);
    #if TIME_COMPUTENODE_EVAL
    gettimeofday(&end, NULL);
    printf("ComputeNode-Check if permutation already explored %f\n", gettimediff(end, begin));
    #endif
    #if OUTPUT_TIMES
    gettimeofday(&end, NULL);
    teval_file_dubchk << std::fixed << std::setprecision(8) << gettimediff(end, begin) << endl;
    #endif
    if (isuniq!=-1)
    {
        node.notUnique = isuniq;
        return;
    }
    
    node.Ms.swap(tmp.Ms);
    node.Mv.swap(tmp.Mv);
    
    #if TIME_COMPUTENODE_EVAL || OUTPUT_TIMES
    gettimeofday(&begin, NULL);
    #endif
    if (params.transfModel==1) updateModel(node, graphA, graphB, params);
    
    #if TIME_COMPUTENODE_EVAL
    gettimeofday(&end, NULL);
    printf("ComputeNode-Update Model %f\n", gettimediff(end, begin));
    #endif
    #if OUTPUT_TIMES
    gettimeofday(&end, NULL);
    teval_file_upmodel << std::fixed << std::setprecision(8) << gettimediff(end, begin) << endl;
    #endif
    #if TIME_COMPUTENODE_EVAL
    gettimeofday(&begin, NULL);
    #endif
    
    int mua = node.possiblept(s,0), mub = node.possiblept(s,1);
    removePossibleSuperedge(node, s);
    
    //any of the possible expandable superedges are using the last added pair
    for (int i=(int)node.possiblese.rows()-1; i>=0; i--)
    {
        if (node.possiblept(i,0) == mua)
            removePossibleSuperedge(node, i);
        else if (node.possiblept(i,1) == mub)
            removePossibleSuperedge(node, i);
    }
    
    if (graphA.superedges[node.Ms(0,newSEcol)].edgeid(0)!=0) //not virtual edge
    {
        //Remove possible superedges which are overlapping current choice
        for (int i=(int)node.possiblese.rows()-1; i>=0; i--)
        {
            VectorXi edges = graphA.superedges[node.possiblese(i,0)].edgeid;
            for (int j=0; j<edges.size(); j++)
                if ( ( graphA.superedges[node.Ms(0,newSEcol)].edgeid.array().abs()==abs(edges(j)) ).any() )
                {
                    removePossibleSuperedge(node, i);
                    break;
                }
        }
        for (int i=(int)node.possiblese.rows()-1; i>=0; i--)
        {
            VectorXi edges = graphB.superedges[node.possiblese(i,1)].edgeid;
            
            for (int j=0; j<edges.size(); j++)
                if ( ( graphB.superedges[node.Ms(1,newSEcol)].edgeid.array().abs()==abs(edges(j)) ).any() )
                {
                    removePossibleSuperedge(node, i);
                    break;
                }
        }
        
        //Remove superedges ending in new skipped nodes from possible superedges
        VectorXi newskippeda = graphA.superedges[node.Ms(0,newSEcol)].path.segment(1, graphA.superedges[node.Ms(0,newSEcol)].path.size()-1),
                 newskippedb = graphB.superedges[node.Ms(1,newSEcol)].path.segment(1, graphB.superedges[node.Ms(1,newSEcol)].path.size()-1);
        
        //don't count the nodes in M_U as skipped
        for (int i=(int)newskippeda.size()-1; i>=0; i--)
            if ( (node.Mv.row(0).array()==newskippeda(i)).any() )
                removeElement(newskippeda, i);
                
        for (int i=(int)newskippedb.size()-1; i>=0; i--)
            if ( (node.Mv.row(1).array()==newskippedb(i)).any() )
                removeElement(newskippedb, i);
        
        for (int i=0; i<newskippeda.size(); i++)
            if ( !(node.skippeda.array()==newskippeda(i)).any() )
            {
                tmp.skippeda = VectorXi( node.skippeda.size()+1 );
                tmp.skippeda.segment(0, node.skippeda.size()) = node.skippeda;
                tmp.skippeda(tmp.skippeda.size()-1) = newskippeda(i);
                node.skippeda.swap(tmp.skippeda);
            }
            
        for (int i=0; i<newskippedb.size(); i++)
            if ( !(node.skippedb.array()==newskippedb(i)).any() )
            {
                tmp.skippedb = VectorXi( node.skippedb.size()+1 );
                tmp.skippedb.segment(0, node.skippedb.size()) = node.skippedb;
                tmp.skippedb(tmp.skippedb.size()-1) = newskippedb(i);
                node.skippedb.swap(tmp.skippedb);
            }
    }
    #if TIME_COMPUTENODE_EVAL
    gettimeofday(&end, NULL);
    printf("ComputeNode-Remove incompatible nodes/possibilities %f\n", gettimediff(end, begin));
    gettimeofday(&begin, NULL);
    #endif
    
    //Add new possible superedge pairs: could be simplified
    vector<int> possiblesea, possiblepta, possibleseb, possibleptb;
    for (int a=0; a<graphA.superedges.size(); a++)
    {
        // if the first endpoint is the one we're adding, the other endpoint is not skipped (otherwise it's overlapping) and not already in M_V and the superedge is not already in M_S
        if ((graphA.superedges[a].path(0)==mua && !(node.skippeda.array()==graphA.superedges[a].path(graphA.superedges[a].path.size()-1)).any() &&
            !(node.Mv.row(0).segment(0, node.Mv.cols()-1).array()==graphA.superedges[a].path(graphA.superedges[a].path.size()-1)).any() &&
            !(node.Ms.row(0).array()==a).any()) && ( graphA.superedges[node.Ms(0,newSEcol)].edgeid(0)==0 || // removed || node.Ms.array()<0).all()
            !(graphA.superedges[a].edgeid.replicate(1, graphA.superedges[node.Ms(0,newSEcol)].edgeid.size()).array().abs()==
            graphA.superedges[node.Ms(0,newSEcol)].edgeid.transpose().replicate(graphA.superedges[a].edgeid.size(), 1).array().abs()).any() ) )
        {
            possiblesea.push_back(a);
            possiblepta.push_back(graphA.superedges[a].path(graphA.superedges[a].path.size()-1));
        }
    }
    for (int b=0; b<graphB.superedges.size(); b++)
        if ((graphB.superedges[b].path(0)==mub && !(node.skippedb.array()==graphB.superedges[b].path(graphB.superedges[b].path.size()-1)).any() &&
            !(node.Mv.row(1).segment(0, node.Mv.cols()-1).array()==graphB.superedges[b].path(graphB.superedges[b].path.size()-1)).any() &&
            !(node.Ms.row(1).array()==b).any()) && ( graphB.superedges[node.Ms(1,newSEcol)].edgeid(0)==0 ||
            !(graphB.superedges[b].edgeid.replicate(1, graphB.superedges[node.Ms(1,newSEcol)].edgeid.size()).array().abs()==
            graphB.superedges[node.Ms(1,newSEcol)].edgeid.transpose().replicate(graphB.superedges[b].edgeid.size(), 1).array().abs()).any() ) )
        {
            possibleseb.push_back(b);
            possibleptb.push_back(graphB.superedges[b].path(graphB.superedges[b].path.size()-1));
        }
    #if IS_TEST
    cout << tmp.Mv.array()+1 << endl;
    for (int a=0; a< possiblepta.size(); a++) printf(" %d", possiblepta[a]+1); printf("\n");
    for (int a=0; a< possibleptb.size(); a++) printf(" %d", possibleptb[a]+1); printf("\n");
    #endif
    #if TIME_COMPUTENODE_EVAL
    gettimeofday(&end, NULL);
    printf("ComputeNode-Get new expandable superedges %f\n", gettimediff(end, begin));
    gettimeofday(&begin, NULL);
    #endif
    
    //meshup
    MatrixXi possiblese = MatrixXi(possiblesea.size()*possibleseb.size(),2);
    MatrixXi possiblept = MatrixXi(possiblepta.size()*possibleptb.size(),2);
    for (int s=0; s<possibleseb.size(); s++)
        for (int a=0; a<possiblesea.size(); a++){
            possiblese(a + s*possiblesea.size(),0) = possiblesea[a];
            possiblese(a + s*possiblesea.size(),1) = possibleseb[s];
            possiblept(a + s*possiblesea.size(),0) = possiblepta[a];
            possiblept(a + s*possiblesea.size(),1) = possibleptb[s];
        }
    
    //Remove incompatible superedges
    for (int s=(int)possiblese.rows()-1; s>=0; s--)
        if (!issecomp(graphA.superedges[possiblese(s,0)].desc, graphB.superedges[possiblese(s,1)].desc, params.epsilon_h) || graphA.fnodes(possiblept(s,0))!=graphB.fnodes(possiblept(s,1)) ||
            (graphA.superedges[possiblese(s,0)].edgeid(0)==0 && graphB.superedges[possiblese(s,1)].edgeid(0)!=0) ||
            (graphA.superedges[possiblese(s,0)].edgeid(0)!=0 && graphB.superedges[possiblese(s,1)].edgeid(0)==0)) //edgeid == 0 -> virtual edge -- it connects nodes in different trees which are within d_c
        {
            
            removeRow(possiblese, s);
            removeRow(possiblept, s);
        }
    #if TIME_COMPUTENODE_EVAL
    gettimeofday(&end, NULL);
    printf("ComputeNode-Mesh and remove incompatible %f\n", gettimediff(end, begin));
    gettimeofday(&begin, NULL);
    #endif
    
    cnt=0;
    if (params.modelCheck==1)
    {
        superedgesToSort.clear();
        for (int m=0; m<node.possiblese.rows(); m++)
        {
            triple se;
            se.index = cnt;
            se.length = graphA.superedges[node.possiblese(m,0)].length + graphB.superedges[node.possiblese(m,1)].length;
            if (graphA.superedges[node.possiblese(m,0)].edgeid(0)==0) //edgeid == 0 -> virtual edge -- it connects nodes in different trees which are within d_c
                se.superedgeSize = params.K+1;
            else
                se.superedgeSize = (int)max(graphA.superedges[node.possiblese(m,0)].edgeid.size(), graphB.superedges[node.possiblese(m,1)].edgeid.size());
            superedgesToSort.push_back(se);
            cnt++;
        }
        for (int m=0; m<possiblese.rows(); m++)
        {
            triple se;
            se.index = cnt;
            se.length = graphA.superedges[possiblese(m,0)].length + graphB.superedges[possiblese(m,1)].length;
            if (graphA.superedges[possiblese(m,0)].edgeid(0)==0) //edgeid == 0 -> virtual edge -- it connects nodes in different trees which are within d_c
                se.superedgeSize = params.K+1;
            else
                se.superedgeSize = (int)max(graphA.superedges[possiblese(m,0)].edgeid.size(), graphB.superedges[possiblese(m,1)].edgeid.size());
            superedgesToSort.push_back(se);
            cnt++;
        }
        
        if (params.ordering == 0)
            sort(superedgesToSort.begin(), superedgesToSort.end(), prioritySort);
    
        tmp.possiblese = MatrixXi( (int)node.possiblese.rows()+possiblese.rows(), 2 );
        tmp.possiblept = MatrixXi( (int)node.possiblese.rows()+possiblese.rows(), 2 );
        for (int m=0; m<tmp.possiblese.rows(); m++)
        {
            if (superedgesToSort[m].index<node.possiblese.rows())
            {
                tmp.possiblese(m,0) = node.possiblese(superedgesToSort[m].index,0);
                tmp.possiblese(m,1) = node.possiblese(superedgesToSort[m].index,1);
                tmp.possiblept(m,0) = node.possiblept(superedgesToSort[m].index,0);
                tmp.possiblept(m,1) = node.possiblept(superedgesToSort[m].index,1);
            }
            else
            {
                tmp.possiblese(m,0) = possiblese(superedgesToSort[m].index-node.possiblese.rows(),0);
                tmp.possiblese(m,1) = possiblese(superedgesToSort[m].index-node.possiblese.rows(),1);
                tmp.possiblept(m,0) = possiblept(superedgesToSort[m].index-node.possiblese.rows(),0);
                tmp.possiblept(m,1) = possiblept(superedgesToSort[m].index-node.possiblese.rows(),1);
            }
        }
        node.possiblese.swap(tmp.possiblese);
        node.possiblept.swap(tmp.possiblept);
        node.priority = VectorXi::Zero(node.possiblese.rows());
        node.seexpanded = VectorXi::Zero(node.possiblese.rows());
    }
    else if(params.modelCheck==0)
    {
        tmp.possiblese = MatrixXi( (int)node.possiblese.rows()+possiblese.rows(), 2 );
        tmp.possiblept = MatrixXi( (int)node.possiblese.rows()+possiblese.rows(), 2 );
        tmp.priority = VectorXi( (int)node.possiblese.rows()+possiblese.rows() );
        tmp.seexpanded = VectorXi::Zero( (int)node.possiblese.rows()+possiblese.rows() );
        
        for (int i=0; i<node.possiblese.rows(); i++)
        {
            tmp.possiblese(cnt,0) = node.possiblese(i,0);
            tmp.possiblese(cnt,1) = node.possiblese(i,1);
            tmp.possiblept(cnt,0) = node.possiblept(i,0);
            tmp.possiblept(cnt,1) = node.possiblept(i,1);
            tmp.priority(cnt) = node.priority(i);
            cnt++;
        }
        node.possiblese.swap(tmp.possiblese);
        node.possiblept.swap(tmp.possiblept);
        node.priority.swap(tmp.priority);
        node.seexpanded.swap(tmp.seexpanded);
        
        for (int i=0; i<possiblese.rows(); i++)
        {
            node.possiblese(cnt,0) = possiblese(i,0);
            node.possiblese(cnt,1) = possiblese(i,1);
            node.possiblept(cnt,0) = possiblept(i,0);
            node.possiblept(cnt,1) = possiblept(i,1);
            if (graphA.superedges[possiblese(i,0)].edgeid(0)==0) //edgeid == 0 -> virtual edge -- it connects nodes in different trees which are within d_c
                node.priority(cnt) = params.K+1;
            else
                node.priority(cnt) = (int)max(graphA.superedges[possiblese(i,0)].edgeid.size(), graphB.superedges[possiblese(i,1)].edgeid.size());
            cnt++;
        }
    }
    #if TIME_COMPUTENODE_EVAL
    gettimeofday(&end, NULL);
    printf("ComputeNode-Add to node new possibilites %f\n", gettimediff(end, begin));
    #endif
    
    #if TIME_COMPUTENODE_EVAL || OUTPUT_TIMES
    gettimeofday(&begin, NULL);
    #endif
    
    #if IS_TEST
        printToMatlabi("Mv", node.Mv);
//        printToMatlabi("Ms", node.Ms);
        printToMatlabi("pts", node.possiblept.transpose());
//        cout << node.possiblese.transpose().array()+1 << endl;
//        printToMatlab("mut", node.trModel.Ct);
    #endif
    
    //Remove possible superedges which are not compatible with transformation model
    if (params.modelCheck==0 && node.possiblept.rows()>0)
        removeModelIncompatible(node, graphA, graphB, params);
    #if TIME_COMPUTENODE_EVAL
    gettimeofday(&end, NULL);
    printf("ComputeNode-Remove incompatible possibilities %f\n", gettimediff(end, begin));
    #endif
    #if OUTPUT_TIMES
    gettimeofday(&end, NULL);
    if (params.transfModel==0) teval_file_eps << std::fixed << std::setprecision(8) << gettimediff(end, begin) << endl;
    if (params.transfModel==1) teval_file_kup << std::fixed << std::setprecision(8) << gettimediff(end, begin) << endl;
    #endif
    
    #if TIME_COMPUTENODE_EVAL
    gettimeofday(&begin, NULL);
    #endif
    
    calculateQ(node, graphA, graphB, avgLen, params);
    node.Qplus = node.Q;
    node.Qtilde = calculateQtilde(tmp, n, Qnorm, params);
    node.visited = 1;
    node.children.clear();
    node.index = mctSize;
    
    #if TIME_COMPUTENODE_EVAL
    gettimeofday(&end, NULL);
    printf("ComputeNode-CalculateQs %f\n", gettimediff(end, begin));
    #endif
}

int selectSuperedge(Node &node, Graph &graphA, Graph &graphB, Params &params) //with best priority (minimum value)
{
    if (params.modelCheck==1)
    {
        #if MODEL_CHECK_DEBUG
        printf("\n");
        #endif
        for (int i=0; i<node.possiblese.rows(); i++)
        {
            #if MODEL_CHECK_DEBUG
            printf("%d-%d,%d,", node.possiblept(i,0)+1, node.possiblept(i,1)+1, (node.seexpanded(i) == 0));
//            cout << node.possiblept(i,0)+1 << "-" << node.possiblept(i,1)+1 << "," << (node.seexpanded(i) == 0) << ",";
            #endif
            if (node.seexpanded(i) == 0 && checkModelCompatibility(node, graphA.nodes.row(node.possiblept(i,0)).transpose(), graphB.nodes.row(node.possiblept(i,1)).transpose(), graphA, graphB, params))
            {
                #if MODEL_CHECK_DEBUG
                printf("\n");
                #endif
                return i;
            }
        }
        return -1;
    }
    else if (params.modelCheck==0)
    {
        int s=-1, minp=params.K+2; //max priority is params.K+1 (we look for min priority)
        for (int i=0; i<node.possiblese.rows(); i++)
        {
            if (node.seexpanded(i) == 1) continue;
            if (node.priority(i) < minp)
            {
                minp = node.priority(i);
                s = i;
            }
            else if(node.priority(i) == minp &&
                graphA.superedges[node.possiblese(i,0)].length + graphB.superedges[node.possiblese(i,1)].length >
                graphA.superedges[node.possiblese(s,0)].length + graphB.superedges[node.possiblese(s,1)].length)
            {
                minp = node.priority(i);
                s = i;
            }
        }
        return s;
    }
    return -1;
}

int selectRootSuperedge(Node &root, Graph &graphA, Graph &graphB, Params &params)
{
    for (int i=0; i<graphA.superedges.size(); i++)
    for (int j=0; j<graphB.superedges.size(); j++)
    {
        if (i==0 && j==0)
        {
            i = root.possiblept(0,0);
            j = root.possiblept(0,1);
        }
        
        // Order not random
        if (params.ordering != 1) {
            if (graphB.superedges[j].edgeid.size() > graphA.superedges[i].edgeid.size()) break;
            while (graphB.superedges[j].edgeid.size() != graphA.superedges[i].edgeid.size() && j<graphB.superedges.size()) j++;
        }
        else if (params.ordering == 1 && graphA.superedges[i].edgeid.size() != graphB.superedges[j].edgeid.size()) {
            continue;
        }
        if (j>=graphB.superedges.size()) return -1;
        
        if (graphA.superedges[i].edgeid(0)==0)
            continue;
        if (graphB.superedges[j].edgeid(0)==0)
            continue;
        
        if (params.ordering == 0 && graphA.superedges[i].length/graphB.superedges[j].length > 1+params.epsilon_h) break;
        
        if (issecomp(graphA.superedges[i].desc, graphB.superedges[j].desc, params.epsilon_h))
        {
            MatrixXi possiblese( root.possiblese.rows()+1, 2 );
            if (root.possiblese.rows()>0)
                possiblese.block(0, 0, root.possiblese.rows(), 2) << root.possiblese;
            possiblese(possiblese.rows()-1,0) = i;
            possiblese(possiblese.rows()-1,1) = j;
            root.possiblese = possiblese;
            
            root.possiblept(0,0) = i;
            root.possiblept(0,1) = j+1;
            
            return (int)root.possiblese.rows()-1;
        }
    }
    return -1;
}

double calculateQtilde(Node &node, int it, double Qnorm, Params &params)
{
    return node.Qplus/Qnorm + params.gamma * sqrt( (2*log(it))/node.visited );
}

void backpropagate(MCST &mct, int c)
{
    int n = mct.parent[c];
    while(n!=-1)
    {
        if (mct.nodes[n].Qplus < mct.nodes[c].Q && !(mct.nodes[c].seexpanded.array()==1).all())
            mct.nodes[n].Qplus = mct.nodes[c].Q;
        //mct.nodes[n].visited++;
        n = mct.parent[n];
    }
}

void calculateQ(Node &node, Graph &graphA, Graph &graphB, double avgLen, Params &params)
{
    node.Q = 0;
    
    for (int i=0; i<node.Ms.cols(); i++)
    {
        if (node.Ms(0,i)==-1 || graphA.superedges[node.Ms(0,i)].edgeid(0)==0) continue;
        node.Q += (graphA.superedges[node.Ms(0,i)].length + graphB.superedges[node.Ms(1,i)].length)/2;
    }
    node.Q += params.kappa * avgLen * node.Mv.cols();
}

void writeResult(char *filename, Node node, double time_elapsed)
{
    ofstream file;
    file.open(filename);
    
    file << "#Time\n";
    file << time_elapsed << endl << endl;
    
    file << "#Mv\n";
    for (int i=0; i<node.Mv.cols(); i++)
        file << node.Mv(0,i)+1 << " " << node.Mv(1,i)+1 << endl;
    file << endl;
    
    file << "#Ms\n";
    for (int i=0; i<node.Ms.cols(); i++)
        file << node.Ms(0,i)+1 << " " << node.Ms(1,i)+1 << endl;
    file << endl;
    
    file << "#Coefficients\n";
    for (int i=0; i<node.trModel.Ct.rows(); i++)
    {
        file << node.trModel.Ct(i,0);
        for (int j=1; j<node.trModel.Ct.cols(); j++)
            file << " " << node.trModel.Ct(i,j); file << endl;
    }
    file << endl;
    
    file.close();
}







