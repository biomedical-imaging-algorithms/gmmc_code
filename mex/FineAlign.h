//
//  FineAlign.h
//  mcts
//
//  Created by Miguel Amável Pinheiro on 3/1/15.
//

#ifndef __mcts__FineAlign__
#define __mcts__FineAlign__

#include <stdio.h>
#include "TransModel.h"

/**
  This function was designed to fine align the graphs, but it is not fully implemented
*/

MatrixXd fineAlign(Graph graphA, Graph graphB, MatrixXd ptsA, MatrixXd ptsB, MatrixXi Mv, MatrixXi Compl_Corr, Params params);

#endif /* defined(__mcts__FineAlign__) */
