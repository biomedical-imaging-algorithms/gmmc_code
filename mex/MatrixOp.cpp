//
//  MatrixOp.cpp
//  mcts
//
//  Created by Miguel Amável Pinheiro on 6/1/15.
//

#include "MatrixOp.h"



void removePossibleSuperedge(Node& node, int superedgeIndex)
{
    removeRow(node.possiblept, superedgeIndex);
    removeRow(node.possiblese, superedgeIndex);
    removeElement(node.seexpanded, superedgeIndex);
    removeElement(node.priority, superedgeIndex);
}

void removeRow(Eigen::MatrixXi& matrix, unsigned int rowToRemove)
{
    unsigned int numRows = (int)matrix.rows()-1;
    unsigned int numCols = (int)matrix.cols();

    if( rowToRemove < numRows )
        matrix.block(rowToRemove,0,numRows-rowToRemove,numCols) = matrix.block(rowToRemove+1,0,numRows-rowToRemove,numCols);

    matrix.conservativeResize(numRows,numCols);
}

void removeColXi(Eigen::MatrixXi& matrix, unsigned int colToRemove)
{
    unsigned int numRows = (int)matrix.rows();
    unsigned int numCols = (int)matrix.cols()-1;

    if( colToRemove < numCols )
        matrix.block(0,colToRemove,numRows,numCols-colToRemove) = matrix.block(0,colToRemove+1,numRows,numCols-colToRemove);

    matrix.conservativeResize(numRows,numCols);
}

void removeColXd(Eigen::MatrixXd& matrix, unsigned int colToRemove)
{
    unsigned int numRows = (int)matrix.rows();
    unsigned int numCols = (int)matrix.cols()-1;

    if( colToRemove < numCols )
        matrix.block(0,colToRemove,numRows,numCols-colToRemove) = matrix.block(0,colToRemove+1,numRows,numCols-colToRemove);

    matrix.conservativeResize(numRows,numCols);
}

void removeElement(VectorXi& vec, unsigned int elemToRemove)
{
    int n = (int)vec.size()-elemToRemove-1;
    vec.segment(elemToRemove,n) = vec.tail(n);
    vec.conservativeResize(vec.size()-1);
}

MatrixXd getDistance(MatrixXd X, MatrixXd Y)
{
    MatrixXd D = MatrixXd::Zero(X.rows(),Y.rows());
    MatrixXd t(X.rows(),Y.rows());
    
    for (int j=0; j<X.cols(); j++) {
        t = (X.col(j).replicate(1,Y.rows()) - Y.col(j).transpose().replicate(X.rows(),1));
        t = t.array().pow(2);
        D = D + t;
    }
    
    return D.array().sqrt();
}

Assignment assignmentsuboptimal(MatrixXd distMatrix)
{
    Assignment res;
    res.ass = (-1)*VectorXi::Ones((int)distMatrix.rows());
    res.cost = 0;
    long r=-1, c=-1;
    double s=0;
    
    while (true)
    {
        s = distMatrix.minCoeff(&r, &c);
        
        if(isfinite(s))
        {
            res.ass(r) = (int)c;
            res.cost += s;
            distMatrix.row(r).setConstant(INFINITY);
            distMatrix.col(c).setConstant(INFINITY);
        }
        else break;
    }
    
    return res;
}

void printToMatlabi(string varname, MatrixXi mat)
{
    printf("%s = [", varname.c_str());
    for (int i=0; i<mat.rows()-1; i++)
    {
        for (int j=0; j<mat.cols(); j++) printf(" %d", mat(i,j)+1);
        printf(";...\n");
    }
    for (int j=0; j<mat.cols(); j++) printf(" %d", mat(mat.rows()-1,j)+1);
    printf("];\n");
}

void printToMatlab(string varname, MatrixXd mat)
{
    printf("%s = [", varname.c_str());
    for (int i=0; i<mat.rows()-1; i++)
    {
        for (int j=0; j<mat.cols(); j++) printf(" %.9f", mat(i,j)); // << setprecision(9) << " " << mat(i,j);
        printf(";...\n");
    }
    for (int j=0; j<mat.cols(); j++) printf(" %.9f", mat(mat.rows()-1,j));
    printf("];\n");
}




