
#include <math.h>
#include <matrix.h>
#include <mex.h>
// #include <Dense>
#include <iostream>
#include "TreeSearch.h"
#include "Graph.h"

using namespace Eigen;

/**
  Interface between MATLAB and C++ code for GMMC algorithm 
*/

Graph readMxGraph(const mxArray *str)
{
	Graph G;
    
    int         field_num;
    mwSize      nelems;
    
    mxArray     *mxNodes, *mxdA, *mxfNodes, *mxsdA, *mxEdgesStruct, *mxSuperEdgesStruct, *mxtmp;
    double      *doubleNodes, *doubledA, *doublefNodes, *doublesdA, *dbltmp;
    
    mwIndex     *Jc, *Ir;
    
    field_num = mxGetFieldNumber(str, "nodes");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field nodes does not exist in one of the graphs\n");
    }
    mxNodes = mxGetFieldByNumber(str, 0, field_num);
    doubleNodes = mxGetPr( mxNodes );
    
    G.nodes = MatrixXd(mxGetM (mxNodes),mxGetN (mxNodes));
    int cnt = 0;
    for (int j=0; j<G.nodes.cols(); j++)
    for (int i=0; i<G.nodes.rows(); i++)
    {
        G.nodes(i,j) = doubleNodes[cnt];
        cnt++;
    }
    G.D = G.nodes.cols();
    
//    field_num = mxGetFieldNumber(str, "dA");
//    mxdA = mxGetFieldByNumber(str, 0, field_num);
//    doubledA = mxGetPr( mxdA );
//    
//    Jc = mxGetJc (mxdA);
//    Ir = mxGetIr (mxdA);
//    
//    G.dA = MatrixXd::Zero(mxGetM (mxdA),mxGetN (mxdA));
//    cnt = 0;
//    for (int j=1; j<G.dA.cols(); j++)
//    for (int i=0; i<Jc[j]-Jc[j-1]; i++)
//    {
//        G.dA(Ir[cnt],j-1) = doubledA[cnt];
//        cnt++;
//    }
    
    field_num = mxGetFieldNumber(str, "fnodes");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field fnodes does not exist in one of the graphs\n");
    }
    mxfNodes = mxGetFieldByNumber(str, 0, field_num);
    doublefNodes = mxGetPr( mxfNodes );
    
    G.fnodes = VectorXi(mxGetM (mxfNodes));
    for (int i=0; i<G.fnodes.rows(); i++)
    {
        G.fnodes(i) = (int)doublefNodes[i];
    }
    
//    field_num = mxGetFieldNumber(str, "sdA");
//    mxsdA = mxGetFieldByNumber(str, 0, field_num);
//    doublesdA = mxGetPr( mxsdA );
//    
//    Jc = mxGetJc (mxsdA);
//    Ir = mxGetIr (mxsdA);
//    
//    G.sdA = MatrixXd::Zero(mxGetM (mxsdA),mxGetN (mxsdA));
//    cnt = 0;
//    for (int j=1; j<G.sdA.cols(); j++)
//    for (int i=0; i<Jc[j]-Jc[j-1]; i++)
//    {
//        G.sdA(Ir[cnt],j-1) = doublesdA[cnt];
//        cnt++;
//    }
    
    // --- Struct edges ---
    field_num = mxGetFieldNumber(str, "edges");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field edges does not exist in one of the graphs\n");
    }
    mxEdgesStruct = mxGetFieldByNumber(str, 0, field_num);
    nelems = mxGetNumberOfElements(mxEdgesStruct);
    
    for (int i=0; i<nelems; i++)
    {
        Edge ed;
        
        field_num = mxGetFieldNumber(mxEdgesStruct, "endpoints");
        if (field_num == -1) {
            mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field endpoints does not exist in an edge\n");
        }
        mxtmp = mxGetFieldByNumber(mxEdgesStruct, i, field_num);
        dbltmp = mxGetPr(mxtmp);
        
        ed.endpoints = VectorXi(mxGetN(mxtmp));
        for(int j=0; j<ed.endpoints.size(); j++)
            ed.endpoints(j) = (int)dbltmp[j]-1;
        
        field_num = mxGetFieldNumber(mxEdgesStruct, "path");
        if (field_num == -1) {
            mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field path does not exist in an edge\n");
        }
        mxtmp = mxGetFieldByNumber(mxEdgesStruct, i, field_num);
        dbltmp = mxGetPr(mxtmp);
        
        cnt=0;
        ed.path = MatrixXd(mxGetM(mxtmp), mxGetN(mxtmp));
        for(int k=0; k<ed.path.cols(); k++)
        for(int j=0; j<ed.path.rows(); j++)
        {
            ed.path(j,k) = dbltmp[cnt];
            cnt++;
        }
        
        field_num = mxGetFieldNumber(mxEdgesStruct, "cumd");
        if (field_num == -1) {
            mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field cumd does not exist in an edge\n");
        }
        mxtmp = mxGetFieldByNumber(mxEdgesStruct, i, field_num);
        dbltmp = mxGetPr(mxtmp);
        
        ed.cumd = VectorXd(mxGetN(mxtmp));
        for(int j=0; j<ed.cumd.size(); j++)
            ed.cumd(j) = dbltmp[j];
        
        field_num = mxGetFieldNumber(mxEdgesStruct, "length");
        if (field_num == -1) {
            mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field length does not exist in an edge\n");
        }
        mxtmp = mxGetFieldByNumber(mxEdgesStruct, i, field_num);
        dbltmp = mxGetPr(mxtmp);
        
        ed.length = dbltmp[0];
        
        G.edges.push_back(ed);
    }
    
    // --- Struct edges ---
    field_num = mxGetFieldNumber(str, "superedges");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field superedges does not exist in one of the graphs\n");
    }
    mxSuperEdgesStruct = mxGetFieldByNumber(str, 0, field_num);
    nelems = mxGetNumberOfElements(mxSuperEdgesStruct);
    
    for (int i=0; i<nelems; i++)
    {
        Superedge sed;
        
        field_num = mxGetFieldNumber(mxSuperEdgesStruct, "path");
        if (field_num == -1) {
            mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field path does not exist in a superedge\n");
        }
        mxtmp = mxGetFieldByNumber(mxSuperEdgesStruct, i, field_num);
        dbltmp = mxGetPr(mxtmp);
        
        sed.path = VectorXi(mxGetN(mxtmp));
        for(int j=0; j<sed.path.size(); j++)
            sed.path(j) = (int)dbltmp[j]-1;
        
        field_num = mxGetFieldNumber(mxSuperEdgesStruct, "edgeid");
        if (field_num == -1) {
            mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field edgeid does not exist in a superedge\n");
        }
        mxtmp = mxGetFieldByNumber(mxSuperEdgesStruct, i, field_num);
        dbltmp = mxGetPr(mxtmp);
        
        sed.edgeid = VectorXi(mxGetN(mxtmp));
        for(int j=0; j<sed.edgeid.size(); j++)
            sed.edgeid(j) = (int)dbltmp[j];
        
        field_num = mxGetFieldNumber(mxSuperEdgesStruct, "length");
        if (field_num == -1) {
            mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field length does not exist in a superedge\n");
        }
        mxtmp = mxGetFieldByNumber(mxSuperEdgesStruct, i, field_num);
        dbltmp = mxGetPr(mxtmp);
        
        sed.length = dbltmp[0];
        
        field_num = mxGetFieldNumber(mxSuperEdgesStruct, "desc");
        if (field_num == -1) {
            mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field desc does not exist in a superedge\n");
        }
        mxtmp = mxGetFieldByNumber(mxSuperEdgesStruct, i, field_num);
        dbltmp = mxGetPr(mxtmp);
        
        sed.desc = VectorXd(mxGetN(mxtmp));
        for(int j=0; j<sed.desc.size(); j++)
            sed.desc(j) = dbltmp[j];
        
        G.superedges.push_back(sed);
    }
    
    return G;
}

Params readMxParams(const mxArray *mxParams)
{
    Params params;
    
    int field_num;
    mxArray *mxtmp;
    double *dbltmp;
    
        
    field_num = mxGetFieldNumber(mxParams, "epsilon_h");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field epsilon_h does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.epsilon_h = dbltmp[0];
        
    field_num = mxGetFieldNumber(mxParams, "epsilon_T");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field epsilon_T does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.epsilon_T = dbltmp[0];
        
    field_num = mxGetFieldNumber(mxParams, "K");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field K does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.K = (int)dbltmp[0];
        
    field_num = mxGetFieldNumber(mxParams, "kappa");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field kappa does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.kappa = dbltmp[0];
        
    field_num = mxGetFieldNumber(mxParams, "gamma");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field gamma does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.gamma = dbltmp[0];
    
        
    field_num = mxGetFieldNumber(mxParams, "N");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field N does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.N = (int)dbltmp[0];
        
    field_num = mxGetFieldNumber(mxParams, "N_exp");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field epsilon_h does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.N_exp = (int)dbltmp[0];
        
    field_num = mxGetFieldNumber(mxParams, "N_sim");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field N_sim does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.N_sim = (int)dbltmp[0];
        
//    field_num = mxGetFieldNumber(mxParams, "N_fine");
//    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
//    dbltmp = mxGetPr(mxtmp);
//
//    params.N_fine = (int)dbltmp[0];
    
    field_num = mxGetFieldNumber(mxParams, "termDepth");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field termDepth does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.termDepth = (int)dbltmp[0];
    
        
    field_num = mxGetFieldNumber(mxParams, "timeLimit");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field timeLimit does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.timeLimit = dbltmp[0];
    
        
    field_num = mxGetFieldNumber(mxParams, "simulateAll");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field simulateAll does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.simulateAll = (bool)dbltmp[0];
    
        
    field_num = mxGetFieldNumber(mxParams, "transfmodel");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field transfmodel does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.transfModel = (int)dbltmp[0];
    
        
    field_num = mxGetFieldNumber(mxParams, "modelcheck");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field modelcheck does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.modelCheck = (int)dbltmp[0];
    
        
    field_num = mxGetFieldNumber(mxParams, "ordering");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field ordering does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.ordering = (int)dbltmp[0];
    
        
    field_num = mxGetFieldNumber(mxParams, "d_m");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field d_m does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.d_m = dbltmp[0];
    
    
    field_num = mxGetFieldNumber(mxParams, "P0_ini");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field P0_ini does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.P0_ini = dbltmp[0];
    
        
    field_num = mxGetFieldNumber(mxParams, "P_initialization");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field P_initialization does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.P_initialization = (int)dbltmp[0];
        
    field_num = mxGetFieldNumber(mxParams, "sigma_r");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field sigma_r does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.sigma_r = VectorXd(mxGetN(mxtmp));
    for(int j=0; j<params.sigma_r.size(); j++)
        params.sigma_r(j) = dbltmp[j];
    
    if ((params.sigma_r.array() == params.sigma_r(0)).all())
        params.noise_dim_independent = true;
    else params.noise_dim_independent = false;
    
    field_num = mxGetFieldNumber(mxParams, "nctrlpts");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field nctrlpts does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    VectorXi nctrlpts = VectorXi(mxGetN(mxtmp));
    for(int j=0; j<nctrlpts.size(); j++)
        nctrlpts(j) = (int)dbltmp[j];
        
    MatrixXd grs(nctrlpts.size(),2);
    for (int i=0; i<nctrlpts.size(); i++)
    {
        grs(i,0) = -2;
        grs(i,1) = 2;
    }
    params.ctrl = controlPoints(grs, nctrlpts);
    
        
    field_num = mxGetFieldNumber(mxParams, "verbose");
    if (field_num == -1) {
        mexErrMsgIdAndTxt("GMMC:UnexistingField", "Error: The field verbose does not exist in params\n");
    }
    mxtmp = mxGetFieldByNumber(mxParams, 0, field_num);
    dbltmp = mxGetPr(mxtmp);

    params.verbose = (int)dbltmp[0];
    
    return params;
}

void printParams(Params params)
{
    mexPrintf("epsilon_T: %f\n", params.epsilon_T);
    mexPrintf("epsilon_h: %f\n", params.epsilon_h);
    mexPrintf("K: %d\n", params.K);
    mexPrintf("kappa: %f\n", params.kappa);
    mexPrintf("gamma: %f\n", params.gamma);
    mexPrintf("N: %d\n", params.N);
    mexPrintf("N_exp: %d\n", params.N_exp);
    mexPrintf("N_sim: %d\n", params.N_sim);
    mexPrintf("termDepth: %d\n", params.termDepth);
    mexPrintf("simulateAll: %d\n", params.simulateAll);
    mexPrintf("transfmodel: %d\n", params.transfModel);
    mexPrintf("modelcheck: %d\n", params.modelCheck);
    mexPrintf("d_m: %f\n", params.d_m);
    mexPrintf("P0_ini: %f\n", params.P0_ini);
    mexPrintf("sigma_r: %f", params.sigma_r(0));
    for (int i=1; i<params.sigma_r.size(); i++) mexPrintf(" %f", params.sigma_r(i));
    mexPrintf("\n");
    mexPrintf("noise_dim_independent: %d\n", params.noise_dim_independent);
    
    mexPrintf("\n");
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    int         nfields;
    
    Params      params;
    Graph       graphA, graphB;
    Node        bestNode;
    MatrixXi    Ps, Compl_Corr;
    MatrixXd    ptsA, ptsB, ptsR;
    
    char filename[128] = "/Users/amavel/Documents/treematching/methods/mcts_cpp/result";
    
    /* check proper input and output */
    if(nrhs!=3)
        mexErrMsgIdAndTxt( "MCTS:run_mcts:invalidNumInputs",
                "Three input required.");
    else if(nlhs > 3)
        mexErrMsgIdAndTxt( "MCTS:run_mcts:maxlhs",
                "Too many output arguments.");
    else if(!mxIsStruct(prhs[0]))
        mexErrMsgIdAndTxt( "MCTS:run_mcts:inputNotStruct",
                "First input must be a structure.");
    else if(!mxIsStruct(prhs[1]))
        mexErrMsgIdAndTxt( "MCTS:run_mcts:inputNotStruct",
                "Second input must be a structure.");
    else if(!mxIsStruct(prhs[2]))
        mexErrMsgIdAndTxt( "MCTS:run_mcts:inputNotStruct",
                "Third input must be a structure.");
    
    /* get input arguments */    
    if (mxGetNumberOfFields(prhs[0])<6 || mxGetNumberOfFields(prhs[1])<6)
        mexErrMsgIdAndTxt( "MCTS:run_mcts:invalidNumFields",
                "Wrong number of fields in struct. 6 are required.");
    
    graphA = readMxGraph(prhs[0]);
    graphB = readMxGraph(prhs[1]);
    
    params = readMxParams(prhs[2]);
    
    if(params.verbose>0) printParams(params);
    
    bestNode = runTreeSearch(graphA, graphB, params, filename);
    
    plhs[0] = mxCreateNumericMatrix(bestNode.Mv.rows(), bestNode.Mv.cols(), mxINT32_CLASS, mxREAL);
    int *Mv = (int *)mxGetData(plhs[0]);
    for (int col=0; col < bestNode.Mv.cols(); col++) {
        for (int row=0; row < bestNode.Mv.rows(); row++) {
            Mv[row + col*bestNode.Mv.rows()] = bestNode.Mv(row,col)+1;
        }
    }
    
    plhs[1] = mxCreateNumericMatrix(bestNode.Ms.rows(), bestNode.Ms.cols(), mxINT32_CLASS, mxREAL);
    int *Ms = (int *)mxGetData(plhs[1]);
    for (int col=0; col < bestNode.Ms.cols(); col++) {
        for (int row=0; row < bestNode.Ms.rows(); row++) {
            Ms[row + col*bestNode.Ms.rows()] = bestNode.Ms(row,col)+1;
        }
    }
    
    plhs[2] = mxCreateDoubleMatrix(1,1,mxREAL);
    double *t = (double *)mxGetData(plhs[2]);
    t[0] = bestNode.time_elapsed;
    
//     mexPrintf("Test: %d\n", graphA.superedges[40].path(2));
//    mexPrintf("Test: %f\n", bestNode.Q);
//    mexPrintf("Test: %d\n", bestNode.Mv.cols());
//     mexPrintf("Test: %f\n", graphB.nodes(0,0));


    
//    mexPrintf("Best solution: Node %d (%f) - %d matches\n", bestNode.index, bestNode.Q, bestNode.Mv.cols());
//    for(int k=0; k<bestNode.Mv.cols(); k++) mexPrintf("%03d ", bestNode.Mv(0,k)+1); mexPrintf("\n");
//    for(int k=0; k<bestNode.Mv.cols(); k++) mexPrintf("%03d ", bestNode.Mv(1,k)+1); mexPrintf("\n");
}














