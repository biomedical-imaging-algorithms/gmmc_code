//
//  MCT.cpp
//  mcts
//
//  Created by Miguel Amável Pinheiro on 24/6/14.
//

#include "MCT.h"


vector<int> recGetChildren(MCST tree, vector<int> indices, int index)
{
    int cnt=0;
    for (int i=index+1; i<tree.nodes.size(); i++)
    {
        if (tree.parent[i]==index)
        {
            indices.push_back(i);
            if (tree.nodes[i].children.size()>0) indices = recGetChildren(tree, indices, i);
            cnt++;
        }
        if (cnt>=tree.nodes[index].children.size()) break;
    }
    return indices;
}

MCST subtree(MCST tree, int index, VectorXi &indxs)
{
    MCST subtree;
    subtree.nodes.reserve( tree.nodes.size() );
    subtree.parent.reserve( tree.nodes.size() );
    
    vector<int> parents;
    vector<int> indices;
    indices.reserve( tree.nodes.size() );
    parents.reserve( tree.nodes.size() );
    
    indices.push_back(index);
    indices = recGetChildren(tree, indices, index);
    
    indxs = VectorXi(indices.size());
    for (int i=0; i<indices.size(); i++)
    {
        subtree.nodes.push_back(tree.nodes[indices[i]]);
        parents.push_back(tree.parent[indices[i]]);
        indxs(i) = indices[i];
    }
    
    subtree.parent.push_back(-1); int pr, j;
    for (int i=1; i<indices.size(); i++)
    {
        pr = parents[i];
        for (j=0; indices[j]!=pr; j++);
        subtree.parent.push_back(j);
    }
    
    return subtree;
}

double gettimediff(struct timeval end, struct timeval begin)
{
    return (((end.tv_sec * 1000000 + end.tv_usec) - (begin.tv_sec * 1000000 + begin.tv_usec))) / 1000000.0;
}



