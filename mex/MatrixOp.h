//
//  MatrixOp.h
//  mcts
//
//  Created by Miguel Amável Pinheiro on 6/1/15.
//

#ifndef __mcts__MatrixOp__
#define __mcts__MatrixOp__

#include <stdio.h>
#include <iomanip> 
#include "MCT.h"

using namespace Eigen;

/**
  struct used in Fine alignment function, not fully implemented
*/
struct Assignment {
    VectorXi ass;
    double cost;
};

/**
  Several functions for matrix operations
*/

void removePossibleSuperedge(Node& node, int superedgeIndex);
void removeRow(Eigen::MatrixXi& matrix, unsigned int rowToRemove);
void removeColXi(Eigen::MatrixXi& matrix, unsigned int colToRemove);
void removeColXd(Eigen::MatrixXd& matrix, unsigned int colToRemove);
void removeElement(VectorXi& vec, unsigned int elemToRemove);

MatrixXd getDistance(MatrixXd X, MatrixXd Y);
Assignment assignmentsuboptimal(MatrixXd distMatrix);

void printToMatlabi(string varname, MatrixXi mat);
void printToMatlab(string varname, MatrixXd mat);

#endif /* defined(__mcts__MatrixOp__) */
