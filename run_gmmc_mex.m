clc;
clear;

addpath('utils');

input_path = 'swc_data/';
swc_input1 = 'pat01_leve_1';
swc_input2 = 'pat01_leve_2';

params.transfmodel  = 0; % Bi-Lipschitz distances (ICIP2015)
% params.transfmodel  = 1; % B-spline transformation

params.verbose = 0;
% params.verbose = 1;

fprintf('Monte Carlo tree search on datasets %s,%s\n', swc_input1, swc_input2);

% force_preprocessing = true;
force_preprocessing = false;

% only_preprocessing = true;
only_preprocessing = false;

% params.interpolfun = 'bspline';
params.interpolfun = 'linear';

% Default Params (read paper for deeper info)
% PreProcessing Params

% Superedge descriptors
params.n_w          = 5;       % Number of sampled points on each edge
params.Omega        = 50;      % Size of descriptor
params.K            = 3;       % Maximum size of superedge (number of edges)

params.sr           = .025;    % Sampling rate for input data
params.d_c          = .15;     % Maximum virtual edge size (graphs are normalized)

% Runtime Params
params.kappa        = .8;      % Weight on reward or objective function
params.gamma        = .01;     % Weight on urgency (for number of times node is visited)
params.N_exp        = 2;       % Number of children added in Expansion
params.N_sim        = 25;      % Number of nodes added in Simulation

params.epsilon_T    = .05;     % Allowed deformation in transformation model
params.epsilon_h    = params.epsilon_T * 3;     % Allowed deformation for superedges
params.N            = 10000;   % Maximum number of iterations
params.termDepth    = 30;      % Number of matches at which the method stops
params.timeLimit    = 100000;  % Maximum time in seconds for the method to stop

params.simulateAll  = true;    % Simulate all expanded nodes

%%%%%%%%%%% When method checks infeasible children %%%%%%%%%%%
% params.modelcheck   = 0; % Check all when adding node
params.modelcheck   = 1; % Check only when selecting child to expand (recommended)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%% How superedges are ordered and prioritized %%%%%%%%%
params.ordering = 0; % Default order
% params.ordering = 1; % Random order
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params = get_gmmc_params(params);

savefile_input1 = [input_path swc_input1 '.mat'];
savefile_input2 = [input_path swc_input2 '.mat'];

if force_preprocessing || ~exist(savefile_input1, 'file') || ~exist(savefile_input2, 'file')
    
    fprintf('Reading swcs...\n');
    filelist = dir([input_path swc_input1 '*.swc']);
    graphX = load_tree([input_path filelist(1).name]);
    for i=2:length(filelist)
        graphX = join_trees(graphX, load_tree([input_path filelist(i).name]));
    end
    filelist = dir([input_path swc_input2 '*.swc']);
    graphY = load_tree([input_path filelist(1).name]);
    for i=2:length(filelist)
        graphY = join_trees(graphY, load_tree([input_path filelist(i).name]));
    end
    
    ws = zeros(params.Omega, params.n_w+2);
    for w=1:params.Omega
        ws(w,:) = [0 sort(rand(1, params.n_w)) 1];
    end
    
    [A,graphXr] = preProcessSingleGraph(graphX, ws, params); %#ok<ASGLU>
    [B,graphYr] = preProcessSingleGraph(graphY, ws, params);
    
    A.fnodes(params.force_branch_A) = 0;
    A.fnodes(params.cropped_nodes_A) = 2;
    B.fnodes(params.force_branch_B) = 0;
    B.fnodes(params.cropped_nodes_B) = 2;
    
    save(savefile_input1, 'A', 'graphXr', 'ws');
    
    A = B; graphXr = graphYr;
    save(savefile_input2, 'A', 'graphXr', 'ws');
    
    if only_preprocessing, return; end %#ok<*UNRCH>
end
load(savefile_input2);
B = A; graphYr = graphXr;
load(savefile_input1);

% Normalize graphs
norm_factorA = max( [max(graphXr.X)-min(graphXr.X)
    max(graphXr.Y)-min(graphXr.Y)
    max(graphXr.Z)-min(graphXr.Z)] );
norm_factorB = max( [max(graphYr.X)-min(graphYr.X)
    max(graphYr.Y)-min(graphYr.Y)
    max(graphYr.Z)-min(graphYr.Z)] );
norm_factor = max([norm_factorA norm_factorB])/2;

[A,graphXr] = normalizeGraph(A, graphXr, norm_factor);
[B,graphYr] = normalizeGraph(B, graphYr, norm_factor);

% Get virtual superedges
A = getVirtualSuperedges(A, params);
B = getVirtualSuperedges(B, params);

if length(params.sigma_r)~=size(A.nodes,2)
    params.sigma_r = params.sigma_r(1).*ones(1,size(A.nodes,2));
end
if length(params.nctrlpts)~=size(A.nodes,2)
    params.nctrlpts = params.nctrlpts(1).*ones(1,size(A.nodes,2));
end

fprintf('Graph A: %d nodes, %d superedges\n', size(A.nodes,1), length(A.superedges));
fprintf('Graph B: %d nodes, %d superedges\n\n', size(B.nodes,1), length(B.superedges));

% return;

if ~exist('mex_run_gmmc', 'file')
    fprintf('Compiling mex...\n');
    compile_gmmc_mex;
    fprintf('Mex function compiled. Running now.\n');
end

% Random order on the superedges
if params.ordering == 1
    sp = A.superedges(cellfun(@(x) x(1)~=0, {A.superedges.edgeid}));
    inds = randperm(length(sp));
    A.superedges(cellfun(@(x) x(1)~=0, {A.superedges.edgeid})) = sp(inds);
    sp = B.superedges(cellfun(@(x) x(1)~=0, {B.superedges.edgeid}));
    inds = randperm(length(sp));
    B.superedges(cellfun(@(x) x(1)~=0, {B.superedges.edgeid})) = sp(inds);
end

[Mv,Ms,t_mcts] = mex_run_gmmc(A, B, params);

Mv = Mv'; Ms = Ms';

fprintf('Elapsed time %fs\n', t_mcts);

plot2gs(A, B, [], Mv');


