function [A,graphXr] = preProcessSingleGraph(swc_graph, ws, params)
% [A,graph] = preProcessSingleGraph(swc_graph, norm_factor, ws, params)
%
% Calculates the representation of the graph from a graph extracted from
% SWC files, including superedges and their descriptors
%
% swc_graph   - graph extracted from SWC files
% graph       - graph struct
% norm_factor - normalizing factor
% ws          - |Omega| x n_w matrix with fixed sample points where the values
%               are in the interval [0,1]
% params      - parameters for the GMMC method
% 

graphXr = swc_graph;

fprintf('Pre-processing graph...\n');

A = preProcGraph(graphXr,params);
A = removeDuplicateNodes(A);

fprintf('Number of nodes: %d\n', size(A.nodes,1));

%%%%%%% Just getting some nice order in this section
%%%%%% We are searching for the matching between the PTS of A and B, not nodes

A.fnodes = ones(size(A.nodes,1),1);

A.fnodes(sum(A.dA,1)) = 0;

%%%%%%%%

fprintf('Getting superedges...\n');
A = getsuperedges(A,params);

fprintf('Getting superedge descriptors...\n');
A = getSuperedgeDescriptors(A,ws);

fprintf('done\n');





