function [A,graphXr] = normalizeGraph(A, graphXr, norm_factor)


avgX = mean(A.nodes);
A.nodes = A.nodes - repmat(avgX, size(A.nodes,1), 1);
A.nodes = A.nodes./norm_factor;

graphXr.X = (graphXr.X - avgX(1))./norm_factor;
graphXr.Y = (graphXr.Y - avgX(2))./norm_factor;
if length(avgX)==3
    graphXr.Z = (graphXr.Z - avgX(3))./norm_factor;
end

for i=1:length(A.edges)
    A.edges(i).path = A.edges(i).path - repmat(avgX', 1, size(A.edges(i).path,2));
    
    A.edges(i).path = A.edges(i).path./norm_factor;
    A.edges(i).cumd = A.edges(i).cumd./norm_factor;
    A.edges(i).length = A.edges(i).length./norm_factor;
end

for i=1:length(A.superedges)
    A.superedges(i).length = A.superedges(i).length./norm_factor;
    A.superedges(i).desc = A.superedges(i).desc./norm_factor;
end


