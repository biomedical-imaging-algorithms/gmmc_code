function params = get_gmmc_params(params)
% params = get_gmmc_params(params)
% 
% Default options for GMMC

if ~isfield(params, 'K') % Max number of edges for a superedge
    params.K = 3;
end
if ~isfield(params, 'N') % Max number of iterations
    params.N = 5000;
end
if ~isfield(params, 'spo') % Number of control points for Bspline curve representation (deprecated)
    params.spo = 4;
end
if ~isfield(params, 'directional') % Directional superedges?
    params.directional = false;
end
if ~isfield(params, 'sr') % sampling rate
    params.sr = .001;
end
if ~isfield(params, 'radius') % node descriptor radius
    params.radius = .2;
end
if ~isfield(params, 'Omega') % Number of elements in path descriptor
    params.Omega = 3;
end
if ~isfield(params, 'n_w') % Number of samples to calculate path descriptor element
    params.n_w = 10;
end
if ~isfield(params, 'leaves') % label all vertices equally
    params.leaves = 0;
end
if ~isfield(params, 'force_branch_A') % force vertex labels in graph A to be branching vertices
    params.force_branch_A = [];
end
if ~isfield(params, 'force_branch_B') % force vertex labels in graph B to be branching vertices
    params.force_branch_B = [];
end
if ~isfield(params, 'cropped_nodes_A') % label vertices in A as cropped edge vertices
    params.cropped_nodes_A = [];
end
if ~isfield(params, 'cropped_nodes_B') % label vertices in B as cropped edge vertices
    params.cropped_nodes_B = [];
end

% B-spline (experimental)
if ~isfield(params, 'P0_ini')
    params.P0_ini = 100;
end
if ~isfield(params, 'sigma_r')
    params.sigma_r = .05^2;
end
if ~isfield(params, 'd_m')
    params.d_m = .1;
end
if ~isfield(params, 'nctrlpts')
    params.nctrlpts = 5;
end
if ~isfield(params, 'P_initialization')
    params.P_initialization = 0;
end


end