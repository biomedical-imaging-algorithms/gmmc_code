function g = getVirtualSuperedges(g, options)

if options.d_c>0
    Dx = pdist2(g.nodes, g.nodes);
    close_nodes = triu(and(Dx<options.d_c, g.hm==Inf));
    [a,b] = find(close_nodes);

    if ~isempty(a)
        cl_spedges(2*length(a)) = g.superedges(1);
        for i=1:length(a)
            x=2*i-1;
            y=2*i;
            cl_spedges(x).path = [a(i) b(i)];
            cl_spedges(x).edgeid = 0;
            cl_spedges(x).length = Dx(a(i),b(i));
            cl_spedges(y).path = [b(i) a(i)];
            cl_spedges(y).edgeid = 0;
            cl_spedges(y).length = Dx(a(i),b(i));
        end
    end
end
% output cl_spedges

% Get descriptors
for i=1:length(cl_spedges)
    cl_spedges(i).desc = cl_spedges(i).length.*ones(1, options.Omega);
end

g.superedges = [g.superedges cl_spedges];

end